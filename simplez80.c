/////////////////////////////////////////////////////////////////////////////////
//
//  SIMPLE Z80 - A Z80 ASSEMBLER
//  By Cayetano Manchón Pernis
//  https://tano.xyz
//  2021
//
//------------------------------------------------------------------------------
//
// The MIT License (MIT)
// 
// Copyright © 2021 
// 
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the “Software”), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////////



#include <limits.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#ifdef WIN_32
#	include <windows.h>
#	include <fileapi.h>
#endif


//
//                          TODO
//----------------------------------------------------------
// 
// Declarations inside bit/res
//
// Declare N bytes or words with `byte[N]`
// 
// Unordered declarations ??
//

//Regular text
#define ANSI_BLK "\e[0;30m"
#define ANSI_RED "\e[0;31m"
#define ANSI_GRN "\e[0;32m"
#define ANSI_YEL "\e[0;33m"
#define ANSI_BLU "\e[0;34m"
#define ANSI_MAG "\e[0;35m"
#define ANSI_CYN "\e[0;36m"
#define ANSI_WHT "\e[0;37m"

//Regular bold text
#define ANSI_BBLK "\e[1;30m"
#define ANSI_BRED "\e[1;31m"
#define ANSI_BGRN "\e[1;32m"
#define ANSI_BYEL "\e[1;33m"
#define ANSI_BBLU "\e[1;34m"
#define ANSI_BMAG "\e[1;35m"
#define ANSI_BCYN "\e[1;36m"
#define ANSI_BWHT "\e[1;37m"

//Regular underline text
#define ANSI_UBLK "\e[4;30m"
#define ANSI_URED "\e[4;31m"
#define ANSI_UGRN "\e[4;32m"
#define ANSI_UYEL "\e[4;33m"
#define ANSI_UBLU "\e[4;34m"
#define ANSI_UMAG "\e[4;35m"
#define ANSI_UCYN "\e[4;36m"
#define ANSI_UWHT "\e[4;37m"

//Regular background
#define ANSI_BLKB "\e[40m"
#define ANSI_REDB "\e[41m"
#define ANSI_GRNB "\e[42m"
#define ANSI_YELB "\e[43m"
#define ANSI_BLUB "\e[44m"
#define ANSI_MAGB "\e[45m"
#define ANSI_CYNB "\e[46m"
#define ANSI_WHTB "\e[47m"

//High intensty background 
#define ANSI_BLKHB "\e[0;100m"
#define ANSI_REDHB "\e[0;101m"
#define ANSI_GRNHB "\e[0;102m"
#define ANSI_YELHB "\e[0;103m"
#define ANSI_BLUHB "\e[0;104m"
#define ANSI_MAGHB "\e[0;105m"
#define ANSI_CYNHB "\e[0;106m"
#define ANSI_WHTHB "\e[0;107m"

//High intensty text
#define ANSI_HBLK "\e[0;90m"
#define ANSI_HRED "\e[0;91m"
#define ANSI_HGRN "\e[0;92m"
#define ANSI_HYEL "\e[0;93m"
#define ANSI_HBLU "\e[0;94m"
#define ANSI_HMAG "\e[0;95m"
#define ANSI_HCYN "\e[0;96m"
#define ANSI_HWHT "\e[0;97m"

//Bold high intensity text
#define ANSI_BHBLK "\e[1;90m"
#define ANSI_BHRED "\e[1;91m"
#define ANSI_BHGRN "\e[1;92m"
#define ANSI_BHYEL "\e[1;93m"
#define ANSI_BHBLU "\e[1;94m"
#define ANSI_BHMAG "\e[1;95m"
#define ANSI_BHCYN "\e[1;96m"
#define ANSI_BHWHT "\e[1;97m"

//Reset
#define ANSI_RST "\e[0m"
typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t   s8;
typedef int16_t  s16;
typedef int32_t  s32;
typedef int64_t  s64;


// Windows compat
#ifdef WIN32


#ifndef PATH_MAX
#	define PATH_MAX 260
#endif

char *
realpath(char *file, char *buffer) {
	if (GetFullPathNameA(file, PATH_MAX, buffer, NULL) != 0) {
		return buffer;
	}
	return NULL;
}


#endif


// Memory macros

#define NEW(T, amount) ((T *)malloc(sizeof(T) * amount))
#define REALLOC(old_ptr, T, amount) ((T *)realloc(old_ptr, sizeof(T) * amount))



typedef enum {
	PLACEHOLDER_TYPE_WORD,
	PLACEHOLDER_TYPE_BYTE,
	PLACEHOLDER_TYPE_REL,
} PlaceholderType;

typedef struct {
	u32 token_id;
	u32 expr_id;
	u16  address;
	PlaceholderType type;
} Placeholder;



typedef enum {
	DECLARATION_TYPE_LABEL,
	DECLARATION_TYPE_EXPRESSION,
} DeclarationType;


typedef struct {
	DeclarationType type;
	u32  token_id;
	char *value;
	u32  value_len;
	u32  item;
	s64  result;
	bool resolved;
} Declaration;

typedef struct {
	u32  includer_id;
	char *filename;
	char *absolute_path;
	char *data_stream;
} File;


typedef enum {
	TOKEN_FLAG_INST        = 1 << 16,
	TOKEN_FLAG_REG         = 1 << 17,
	TOKEN_FLAG_REG_HALF    = 1 << 18,
	TOKEN_FLAG_REG_FULL    = 1 << 19,
	TOKEN_FLAG_EXPR        = 1 << 20,
	TOKEN_FLAG_OPER_BINARY = 1 << 21,
	TOKEN_FLAG_OPER_UNARY  = 1 << 22,
	TOKEN_FLAG_DIRECTIVE   = 1 << 23,
	TOKEN_FLAG_END_OF_STATEMENT = 1 << 24,
	TOKEN_FLAG_ASM_STATEMENT    = 1 << 25,
	TOKEN_FLAG_FLAG             = 1 << 26,
} TokeFlag;



typedef enum {
	TOKEN_ENUM_EOF            = 0,
	TOKEN_ENUM_EOL            ,
	TOKEN_ENUM_IDENTIFIER     ,
	TOKEN_ENUM_NUMBER         ,
	TOKEN_ENUM_UNKNOWN        ,
	TOKEN_ENUM_COLON          ,
	TOKEN_ENUM_SEMICOLON      ,
	TOKEN_ENUM_COMMA          ,
	TOKEN_ENUM_DQUOTE         ,
	TOKEN_ENUM_PLUS           ,
	TOKEN_ENUM_MINUS          ,
	TOKEN_ENUM_ASTERISK       ,
	TOKEN_ENUM_SLASH          ,
	TOKEN_ENUM_AMPERSAND      ,
	TOKEN_ENUM_PIPE           ,
	TOKEN_ENUM_R_DISP         ,
	TOKEN_ENUM_L_DISP         ,
	TOKEN_ENUM_TILDE          , // This: ~
	TOKEN_ENUM_CARET          , // This: ^
	TOKEN_ENUM_OPEN_PARENTHESIS,
	TOKEN_ENUM_CLOSE_PARENTHESIS,
	TOKEN_ENUM_OPEN_BRACKETS  ,
	TOKEN_ENUM_CLOSE_BRACKETS ,
	TOKEN_ENUM_ASSIGNMENT     ,
	TOKEN_ENUM_CURRENT_ADDR   ,

	TOKEN_ENUM_REG_AF         = 500,
	TOKEN_ENUM_REG_BC         ,
	TOKEN_ENUM_REG_DE         ,
	TOKEN_ENUM_REG_HL         ,
	TOKEN_ENUM_REG_SP         ,
	TOKEN_ENUM_REG_IX         ,
	TOKEN_ENUM_REG_IY         ,
	TOKEN_ENUM_REG_A          ,
	TOKEN_ENUM_REG_B          ,
	TOKEN_ENUM_REG_C          , // This is a bit weird because this is also valid for the carry condition in 'jr c' for example. 
	TOKEN_ENUM_REG_D          ,
	TOKEN_ENUM_REG_E          ,
	TOKEN_ENUM_REG_H          ,
	TOKEN_ENUM_REG_L          ,
	TOKEN_ENUM_REG_I          ,
	TOKEN_ENUM_REG_R          ,
	TOKEN_ENUM_REG_IXH        ,
	TOKEN_ENUM_REG_IXL        ,
	TOKEN_ENUM_REG_IYH        ,
	TOKEN_ENUM_REG_IYL        ,

	TOKEN_ENUM_KWORD_BYTE           = 600,
	TOKEN_ENUM_KWORD_WORD           ,
	TOKEN_ENUM_KWORD_STRING         ,
	TOKEN_ENUM_KWORD_STRINGZ        ,


	TOKEN_ENUM_INST_JP        = 620,
	TOKEN_ENUM_INST_JR        ,
	TOKEN_ENUM_INST_DJNZ      ,
	TOKEN_ENUM_INST_DEC       ,
	TOKEN_ENUM_INST_INC       ,
	TOKEN_ENUM_INST_ADD       ,
	TOKEN_ENUM_INST_SUB       ,
	TOKEN_ENUM_INST_ADC       ,
	TOKEN_ENUM_INST_SBC       ,
	TOKEN_ENUM_INST_OR        ,
	TOKEN_ENUM_INST_AND       ,
	TOKEN_ENUM_INST_XOR       ,
	TOKEN_ENUM_INST_CP        ,
	TOKEN_ENUM_INST_LD        ,
	TOKEN_ENUM_INST_RET       ,
	TOKEN_ENUM_INST_CALL      ,
	TOKEN_ENUM_INST_RST       ,
	TOKEN_ENUM_INST_OUT       ,
	TOKEN_ENUM_INST_IN        ,
	TOKEN_ENUM_INST_PUSH      ,
	TOKEN_ENUM_INST_POP       ,
	TOKEN_ENUM_INST_EX        ,
	TOKEN_ENUM_INST_IM        ,
	TOKEN_ENUM_INST_RLC       ,
	TOKEN_ENUM_INST_RRC       ,
	TOKEN_ENUM_INST_RL        ,
	TOKEN_ENUM_INST_RR        ,
	TOKEN_ENUM_INST_SLA       ,
	TOKEN_ENUM_INST_SRA       ,
	TOKEN_ENUM_INST_SLL       ,
	TOKEN_ENUM_INST_SRL       ,
	TOKEN_ENUM_INST_BIT       ,
	TOKEN_ENUM_INST_RES       ,
	TOKEN_ENUM_INST_SET       ,
	TOKEN_ENUM_INST_NOP       ,
	TOKEN_ENUM_INST_DI        ,
	TOKEN_ENUM_INST_EI        ,
	TOKEN_ENUM_INST_EXX       ,
	TOKEN_ENUM_INST_RLCA      ,
	TOKEN_ENUM_INST_RRCA      ,
	TOKEN_ENUM_INST_RLA       ,
	TOKEN_ENUM_INST_RRA       ,
	TOKEN_ENUM_INST_DAA       ,
	TOKEN_ENUM_INST_CPL       ,
	TOKEN_ENUM_INST_SCF       ,
	TOKEN_ENUM_INST_CCF       ,
	TOKEN_ENUM_INST_HALT      ,
	TOKEN_ENUM_INST_RETN      ,
	TOKEN_ENUM_INST_NEG       ,
	TOKEN_ENUM_INST_OUTI      ,
	TOKEN_ENUM_INST_OUTD      ,
	TOKEN_ENUM_INST_RRD       ,
	TOKEN_ENUM_INST_RLD       ,
	TOKEN_ENUM_INST_LDI       ,
	TOKEN_ENUM_INST_CPI       ,
	TOKEN_ENUM_INST_INI       ,
	TOKEN_ENUM_INST_LDD       ,
	TOKEN_ENUM_INST_CPD       ,
	TOKEN_ENUM_INST_IND       ,
	TOKEN_ENUM_INST_LDIR      ,
	TOKEN_ENUM_INST_CPIR      ,
	TOKEN_ENUM_INST_INIR      ,
	TOKEN_ENUM_INST_OTIR      ,
	TOKEN_ENUM_INST_LDDR      ,
	TOKEN_ENUM_INST_CPDR      ,
	TOKEN_ENUM_INST_INDR      ,
	TOKEN_ENUM_INST_OTDR      ,

	TOKEN_ENUM_STRING         ,

	TOKEN_ENUM_DIR_INCLUDE    ,
	TOKEN_ENUM_DIR_COMPFLAGS  ,
	
	TOKEN_ENUM_FLAG_START_ADDR  ,
	TOKEN_ENUM_FLAG_OUT ,
	TOKEN_ENUM_FLAG_CPC_SNA ,
	TOKEN_ENUM_FLAG_NOI ,
} TokenEnum;

typedef enum {
	TOKEN_TYPE_EOF            = TOKEN_ENUM_EOF            ,
	TOKEN_TYPE_EOL            = TOKEN_FLAG_END_OF_STATEMENT | TOKEN_ENUM_EOL,
	TOKEN_TYPE_UNKNOWN        = TOKEN_ENUM_UNKNOWN        ,
	TOKEN_TYPE_COLON          = TOKEN_ENUM_COLON          ,
	TOKEN_TYPE_SEMICOLON      = TOKEN_FLAG_END_OF_STATEMENT | TOKEN_ENUM_SEMICOLON,
	TOKEN_TYPE_COMMA          = TOKEN_ENUM_COMMA          ,
	TOKEN_TYPE_DQUOTE         = TOKEN_ENUM_DQUOTE         ,
	TOKEN_TYPE_OPEN_BRACKETS  = TOKEN_ENUM_OPEN_BRACKETS  ,
	TOKEN_TYPE_CLOSE_BRACKETS = TOKEN_ENUM_CLOSE_BRACKETS ,
	
	TOKEN_TYPE_IDENTIFIER   = TOKEN_FLAG_EXPR | TOKEN_ENUM_IDENTIFIER ,
	TOKEN_TYPE_NUMBER       = TOKEN_FLAG_EXPR | TOKEN_ENUM_NUMBER     ,
	TOKEN_TYPE_CURRENT_ADDR = TOKEN_FLAG_EXPR | TOKEN_ENUM_CURRENT_ADDR,
	TOKEN_TYPE_PLUS         = TOKEN_FLAG_EXPR | TOKEN_FLAG_OPER_BINARY | TOKEN_FLAG_OPER_UNARY | TOKEN_ENUM_PLUS ,
	TOKEN_TYPE_MINUS        = TOKEN_FLAG_EXPR | TOKEN_FLAG_OPER_BINARY | TOKEN_FLAG_OPER_UNARY | TOKEN_ENUM_MINUS,
	TOKEN_TYPE_ASTERISK     = TOKEN_FLAG_EXPR | TOKEN_FLAG_OPER_BINARY | TOKEN_ENUM_ASTERISK,
	TOKEN_TYPE_SLASH        = TOKEN_FLAG_EXPR | TOKEN_FLAG_OPER_BINARY | TOKEN_ENUM_SLASH   ,
	TOKEN_TYPE_OPEN_PARENTHESIS  = TOKEN_FLAG_EXPR | TOKEN_ENUM_OPEN_PARENTHESIS,
	TOKEN_TYPE_CLOSE_PARENTHESIS = TOKEN_FLAG_EXPR | TOKEN_ENUM_CLOSE_BRACKETS,

	
	TOKEN_TYPE_AMPERSAND = TOKEN_FLAG_EXPR | TOKEN_FLAG_OPER_BINARY | TOKEN_ENUM_AMPERSAND ,
	TOKEN_TYPE_PIPE      = TOKEN_FLAG_EXPR | TOKEN_FLAG_OPER_BINARY | TOKEN_ENUM_PIPE      ,
	TOKEN_TYPE_R_DISP    = TOKEN_FLAG_EXPR | TOKEN_FLAG_OPER_BINARY | TOKEN_ENUM_R_DISP    ,
	TOKEN_TYPE_L_DISP    = TOKEN_FLAG_EXPR | TOKEN_FLAG_OPER_BINARY | TOKEN_ENUM_L_DISP    ,
	TOKEN_TYPE_CARET     = TOKEN_FLAG_EXPR | TOKEN_FLAG_OPER_BINARY | TOKEN_ENUM_CARET     , // This: ^
	TOKEN_TYPE_TILDE     = TOKEN_FLAG_EXPR | TOKEN_FLAG_OPER_UNARY  | TOKEN_ENUM_TILDE     , // This: ~

	TOKEN_TYPE_ASSIGNMENT= TOKEN_ENUM_ASSIGNMENT,

	TOKEN_TYPE_REG_AF     = TOKEN_FLAG_REG | TOKEN_FLAG_REG_FULL | TOKEN_ENUM_REG_AF ,
	TOKEN_TYPE_REG_BC     = TOKEN_FLAG_REG | TOKEN_FLAG_REG_FULL | TOKEN_ENUM_REG_BC ,
	TOKEN_TYPE_REG_DE     = TOKEN_FLAG_REG | TOKEN_FLAG_REG_FULL | TOKEN_ENUM_REG_DE ,
	TOKEN_TYPE_REG_HL     = TOKEN_FLAG_REG | TOKEN_FLAG_REG_FULL | TOKEN_ENUM_REG_HL ,
	TOKEN_TYPE_REG_SP     = TOKEN_FLAG_REG | TOKEN_FLAG_REG_FULL | TOKEN_ENUM_REG_SP ,
	TOKEN_TYPE_REG_IX     = TOKEN_FLAG_REG | TOKEN_FLAG_REG_FULL | TOKEN_ENUM_REG_IX ,
	TOKEN_TYPE_REG_IY     = TOKEN_FLAG_REG | TOKEN_FLAG_REG_FULL | TOKEN_ENUM_REG_IY ,
	TOKEN_TYPE_REG_A      = TOKEN_FLAG_REG | TOKEN_FLAG_REG_HALF | TOKEN_ENUM_REG_A  ,
	TOKEN_TYPE_REG_B      = TOKEN_FLAG_REG | TOKEN_FLAG_REG_HALF | TOKEN_ENUM_REG_B  ,
	TOKEN_TYPE_REG_C      = TOKEN_FLAG_REG | TOKEN_FLAG_REG_HALF | TOKEN_ENUM_REG_C  ,
	TOKEN_TYPE_REG_D      = TOKEN_FLAG_REG | TOKEN_FLAG_REG_HALF | TOKEN_ENUM_REG_D  ,
	TOKEN_TYPE_REG_E      = TOKEN_FLAG_REG | TOKEN_FLAG_REG_HALF | TOKEN_ENUM_REG_E  ,
	TOKEN_TYPE_REG_H      = TOKEN_FLAG_REG | TOKEN_FLAG_REG_HALF | TOKEN_ENUM_REG_H  ,
	TOKEN_TYPE_REG_L      = TOKEN_FLAG_REG | TOKEN_FLAG_REG_HALF | TOKEN_ENUM_REG_L  ,
	TOKEN_TYPE_REG_I      = TOKEN_FLAG_REG | TOKEN_FLAG_REG_HALF | TOKEN_ENUM_REG_I  ,
	TOKEN_TYPE_REG_R      = TOKEN_FLAG_REG | TOKEN_FLAG_REG_HALF | TOKEN_ENUM_REG_R  ,
	TOKEN_TYPE_REG_IXH    = TOKEN_FLAG_REG | TOKEN_FLAG_REG_HALF | TOKEN_ENUM_REG_IXH,
	TOKEN_TYPE_REG_IXL    = TOKEN_FLAG_REG | TOKEN_FLAG_REG_HALF | TOKEN_ENUM_REG_IXL,
	TOKEN_TYPE_REG_IYH    = TOKEN_FLAG_REG | TOKEN_FLAG_REG_HALF | TOKEN_ENUM_REG_IYH,
	TOKEN_TYPE_REG_IYL    = TOKEN_FLAG_REG | TOKEN_FLAG_REG_HALF | TOKEN_ENUM_REG_IYL,

	TOKEN_TYPE_KWORD_BYTE    = TOKEN_FLAG_ASM_STATEMENT | TOKEN_ENUM_KWORD_BYTE,
	TOKEN_TYPE_KWORD_WORD    = TOKEN_FLAG_ASM_STATEMENT | TOKEN_ENUM_KWORD_WORD,
	TOKEN_TYPE_KWORD_STRING  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_ENUM_KWORD_STRING,
	TOKEN_TYPE_KWORD_STRINGZ = TOKEN_FLAG_ASM_STATEMENT | TOKEN_ENUM_KWORD_STRINGZ,

	TOKEN_TYPE_INST_JP   = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_JP  ,
	TOKEN_TYPE_INST_JR   = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_JR  ,
	TOKEN_TYPE_INST_DJNZ = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_DJNZ,
	TOKEN_TYPE_INST_DEC  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_DEC ,
	TOKEN_TYPE_INST_INC  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_INC ,
	TOKEN_TYPE_INST_ADD  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_ADD ,
	TOKEN_TYPE_INST_SUB  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_SUB ,
	TOKEN_TYPE_INST_ADC  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_ADC ,
	TOKEN_TYPE_INST_SBC  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_SBC ,
	TOKEN_TYPE_INST_OR   = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_OR  ,
	TOKEN_TYPE_INST_AND  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_AND ,
	TOKEN_TYPE_INST_XOR  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_XOR ,
	TOKEN_TYPE_INST_CP   = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_CP  ,
	TOKEN_TYPE_INST_LD   = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_LD  ,
	TOKEN_TYPE_INST_RET  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_RET ,
	TOKEN_TYPE_INST_CALL = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_CALL,
	TOKEN_TYPE_INST_RST  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_RST ,
	TOKEN_TYPE_INST_OUT  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_OUT ,
	TOKEN_TYPE_INST_IN   = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_IN  ,
	TOKEN_TYPE_INST_PUSH = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_PUSH,
	TOKEN_TYPE_INST_POP  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_POP ,
	TOKEN_TYPE_INST_EX   = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_EX  ,
	TOKEN_TYPE_INST_IM   = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_IM  ,
	TOKEN_TYPE_INST_RLC  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_RLC ,
	TOKEN_TYPE_INST_RRC  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_RRC ,
	TOKEN_TYPE_INST_RL   = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_RL  ,
	TOKEN_TYPE_INST_RR   = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_RR  ,
	TOKEN_TYPE_INST_SLA  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_SLA ,
	TOKEN_TYPE_INST_SRA  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_SRA ,
	TOKEN_TYPE_INST_SLL  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_SLL ,
	TOKEN_TYPE_INST_SRL  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_SRL ,
	TOKEN_TYPE_INST_BIT  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_BIT ,
	TOKEN_TYPE_INST_RES  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_RES ,
	TOKEN_TYPE_INST_SET  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_SET ,
	TOKEN_TYPE_INST_NOP  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_NOP ,
	TOKEN_TYPE_INST_DI   = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_DI  ,
	TOKEN_TYPE_INST_EI   = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_EI  ,
	TOKEN_TYPE_INST_EXX  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_EXX ,
	TOKEN_TYPE_INST_RLCA = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_RLCA,
	TOKEN_TYPE_INST_RRCA = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_RRCA,
	TOKEN_TYPE_INST_RLA  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_RLA ,
	TOKEN_TYPE_INST_RRA  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_RRA ,
	TOKEN_TYPE_INST_DAA  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_DAA ,
	TOKEN_TYPE_INST_CPL  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_CPL ,
	TOKEN_TYPE_INST_SCF  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_SCF ,
	TOKEN_TYPE_INST_CCF  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_CCF ,
	TOKEN_TYPE_INST_HALT = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_HALT,
	TOKEN_TYPE_INST_RETN = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_RETN,
	TOKEN_TYPE_INST_NEG  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_NEG ,
	TOKEN_TYPE_INST_OUTI = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_OUTI,
	TOKEN_TYPE_INST_OUTD = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_OUTD,
	TOKEN_TYPE_INST_RRD  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_RRD ,
	TOKEN_TYPE_INST_RLD  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_RLD ,
	TOKEN_TYPE_INST_LDI  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_LDI ,
	TOKEN_TYPE_INST_CPI  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_CPI ,
	TOKEN_TYPE_INST_INI  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_INI ,
	TOKEN_TYPE_INST_LDD  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_LDD ,
	TOKEN_TYPE_INST_CPD  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_CPD ,
	TOKEN_TYPE_INST_IND  = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_IND ,
	TOKEN_TYPE_INST_LDIR = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_LDIR,
	TOKEN_TYPE_INST_CPIR = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_CPIR,
	TOKEN_TYPE_INST_INIR = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_INIR,
	TOKEN_TYPE_INST_OTIR = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_OTIR,
	TOKEN_TYPE_INST_LDDR = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_LDDR,
	TOKEN_TYPE_INST_CPDR = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_CPDR,
	TOKEN_TYPE_INST_INDR = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_INDR,
	TOKEN_TYPE_INST_OTDR = TOKEN_FLAG_ASM_STATEMENT | TOKEN_FLAG_INST | TOKEN_ENUM_INST_OTDR,

	TOKEN_TYPE_STRING    = TOKEN_ENUM_STRING,

	TOKEN_TYPE_DIR_INCLUDE   = TOKEN_FLAG_DIRECTIVE | TOKEN_ENUM_DIR_INCLUDE,
	TOKEN_TYPE_DIR_COMPFLAGS = TOKEN_FLAG_DIRECTIVE | TOKEN_ENUM_DIR_COMPFLAGS,

	TOKEN_TYPE_FLAG_START_ADDR = TOKEN_FLAG_FLAG | TOKEN_ENUM_FLAG_START_ADDR,
	TOKEN_TYPE_FLAG_OUT        = TOKEN_FLAG_FLAG | TOKEN_ENUM_FLAG_OUT,
	TOKEN_TYPE_FLAG_CPC_SNA    = TOKEN_FLAG_FLAG | TOKEN_ENUM_FLAG_CPC_SNA,
	TOKEN_TYPE_FLAG_NOI        = TOKEN_FLAG_FLAG | TOKEN_ENUM_FLAG_NOI,

} TokenType;


char *token_type_strings[] = {
	[TOKEN_ENUM_EOF]            = "End of file",
	[TOKEN_ENUM_IDENTIFIER]     = "Identifier",
	[TOKEN_ENUM_NUMBER]         = "Number",
	[TOKEN_ENUM_UNKNOWN]        = "Unknown",
	[TOKEN_ENUM_COLON]          = ":",
	[TOKEN_ENUM_SEMICOLON]      = ";",
	[TOKEN_ENUM_COMMA]          = ",",
	[TOKEN_ENUM_DQUOTE]         = "\"",
	[TOKEN_ENUM_PLUS]           = "+",
	[TOKEN_ENUM_MINUS]          = "-",
	[TOKEN_ENUM_OPEN_BRACKETS]  = "[",
	[TOKEN_ENUM_CLOSE_BRACKETS] = "]",
	[TOKEN_ENUM_CURRENT_ADDR]   = "$",

	[TOKEN_ENUM_REG_AF ] = "af",
	[TOKEN_ENUM_REG_BC ] = "bc",
	[TOKEN_ENUM_REG_DE ] = "de",
	[TOKEN_ENUM_REG_HL ] = "hl",
	[TOKEN_ENUM_REG_SP ] = "sp",
	[TOKEN_ENUM_REG_IX ] = "ix",
	[TOKEN_ENUM_REG_IY ] = "iy",
	[TOKEN_ENUM_REG_A  ] = "a",
	[TOKEN_ENUM_REG_B  ] = "b",
	[TOKEN_ENUM_REG_C  ] = "c",
	[TOKEN_ENUM_REG_D  ] = "d",
	[TOKEN_ENUM_REG_E  ] = "e",
	[TOKEN_ENUM_REG_H  ] = "h",
	[TOKEN_ENUM_REG_L  ] = "l",
	[TOKEN_ENUM_REG_I  ] = "i",
	[TOKEN_ENUM_REG_R  ] = "r",
	[TOKEN_ENUM_REG_IXH] = "ixh",
	[TOKEN_ENUM_REG_IXL] = "ixl",
	[TOKEN_ENUM_REG_IYH] = "iyh",
	[TOKEN_ENUM_REG_IYL] = "iyl",

	[TOKEN_ENUM_KWORD_BYTE]    = "byte",
	[TOKEN_ENUM_KWORD_WORD]    = "word",
	[TOKEN_ENUM_KWORD_STRING]  = "string",
	[TOKEN_ENUM_KWORD_STRINGZ] = "stringz",

	[TOKEN_ENUM_INST_JP  ] = "jp",  
	[TOKEN_ENUM_INST_JR  ] = "jr",  
	[TOKEN_ENUM_INST_DJNZ] = "djnz",
	[TOKEN_ENUM_INST_DEC ] = "dec", 
	[TOKEN_ENUM_INST_INC ] = "inc", 
	[TOKEN_ENUM_INST_ADD ] = "add", 
	[TOKEN_ENUM_INST_SUB ] = "sub", 
	[TOKEN_ENUM_INST_ADC ] = "adc", 
	[TOKEN_ENUM_INST_SBC ] = "sbc", 
	[TOKEN_ENUM_INST_OR  ] = "or",  
	[TOKEN_ENUM_INST_AND ] = "and", 
	[TOKEN_ENUM_INST_XOR ] = "xor", 
	[TOKEN_ENUM_INST_CP  ] = "cp",  
	[TOKEN_ENUM_INST_LD  ] = "ld",  
	[TOKEN_ENUM_INST_RET ] = "ret", 
	[TOKEN_ENUM_INST_CALL] = "call",
	[TOKEN_ENUM_INST_RST ] = "rst", 
	[TOKEN_ENUM_INST_OUT ] = "out", 
	[TOKEN_ENUM_INST_IN  ] = "in",  
	[TOKEN_ENUM_INST_PUSH] = "push",
	[TOKEN_ENUM_INST_POP ] = "pop", 
	[TOKEN_ENUM_INST_EX  ] = "ex",  
	[TOKEN_ENUM_INST_IM  ] = "im",  
	[TOKEN_ENUM_INST_RLC ] = "rlc", 
	[TOKEN_ENUM_INST_RRC ] = "rrc", 
	[TOKEN_ENUM_INST_RL  ] = "rl",  
	[TOKEN_ENUM_INST_RR  ] = "rr",  
	[TOKEN_ENUM_INST_SLA ] = "sla", 
	[TOKEN_ENUM_INST_SRA ] = "sra", 
	[TOKEN_ENUM_INST_SLL ] = "sll", 
	[TOKEN_ENUM_INST_SRL ] = "srl", 
	[TOKEN_ENUM_INST_BIT ] = "bit", 
	[TOKEN_ENUM_INST_RES ] = "res", 
	[TOKEN_ENUM_INST_SET ] = "set", 
	[TOKEN_ENUM_INST_NOP ] = "nop", 
	[TOKEN_ENUM_INST_DI  ] = "di",
	[TOKEN_ENUM_INST_EI  ] = "ei",
	[TOKEN_ENUM_INST_EXX ] = "exx", 
	[TOKEN_ENUM_INST_RLCA] = "rlca",
	[TOKEN_ENUM_INST_RRCA] = "rrca",
	[TOKEN_ENUM_INST_RLA ] = "rla", 
	[TOKEN_ENUM_INST_RRA ] = "rra", 
	[TOKEN_ENUM_INST_DAA ] = "daa", 
	[TOKEN_ENUM_INST_CPL ] = "cpl", 
	[TOKEN_ENUM_INST_SCF ] = "scf", 
	[TOKEN_ENUM_INST_CCF ] = "ccf", 
	[TOKEN_ENUM_INST_HALT] = "halt",
	[TOKEN_ENUM_INST_RETN] = "retn",
	[TOKEN_ENUM_INST_NEG ] = "neg", 
	[TOKEN_ENUM_INST_OUTI] = "outi",
	[TOKEN_ENUM_INST_OUTD] = "outd",
	[TOKEN_ENUM_INST_RRD ] = "rrd", 
	[TOKEN_ENUM_INST_RLD ] = "rld", 
	[TOKEN_ENUM_INST_LDI ] = "ldi", 
	[TOKEN_ENUM_INST_CPI ] = "cpi", 
	[TOKEN_ENUM_INST_INI ] = "ini", 
	[TOKEN_ENUM_INST_LDD ] = "ldd", 
	[TOKEN_ENUM_INST_CPD ] = "cpd", 
	[TOKEN_ENUM_INST_IND ] = "ind", 
	[TOKEN_ENUM_INST_LDIR] = "ldir",
	[TOKEN_ENUM_INST_CPIR] = "cpir",
	[TOKEN_ENUM_INST_INIR] = "inir",
	[TOKEN_ENUM_INST_OTIR] = "otir",
	[TOKEN_ENUM_INST_LDDR] = "lddr",
	[TOKEN_ENUM_INST_CPDR] = "cpdr",
	[TOKEN_ENUM_INST_INDR] = "indr",
	[TOKEN_ENUM_INST_OTDR] = "otdr",
	
	[TOKEN_ENUM_STRING] = "string",

	[TOKEN_ENUM_DIR_INCLUDE]   = "#include",
	[TOKEN_ENUM_DIR_COMPFLAGS] = "#compflags",

	[TOKEN_ENUM_FLAG_START_ADDR] = "--start-addr",
	[TOKEN_ENUM_FLAG_OUT]        = "--out",
	[TOKEN_ENUM_FLAG_CPC_SNA]    = "--cpc-sna",
	[TOKEN_ENUM_FLAG_NOI]        = "--noi",
};


typedef struct {
	TokenType type;
	char *value;
	u32  value_len;
	u32  file_id;
	u32  line;
	u32  column;
	u32  line_index;
} Token;

typedef struct {
	u32 file_id;
	u32 line;
	u32 column;
	char *stream;
	u32 stream_index;
	u32 stream_line_index;
	char *filename;
} Tokenizer;



typedef struct {
	u32 begin;
	u32 current;
	u32 end;
} TokenIter;


typedef enum {
	EXPRESSION_ENUM_ADD,
	EXPRESSION_ENUM_SUB,
	EXPRESSION_ENUM_MUL,
	EXPRESSION_ENUM_DIV,
	EXPRESSION_ENUM_AND,
	EXPRESSION_ENUM_OR,
	EXPRESSION_ENUM_XOR,
	EXPRESSION_ENUM_R_DISP,
	EXPRESSION_ENUM_L_DISP,
	EXPRESSION_ENUM_UNARY_MINUS,
	EXPRESSION_ENUM_UNARY_PLUS,
	EXPRESSION_ENUM_INVERT,
	EXPRESSION_ENUM_SUBEXPRESSION,
	EXPRESSION_ENUM_NUMBER,
	EXPRESSION_ENUM_DECLARATION,
	EXPRESSION_ENUM_CURRENT_ADDRESS,
} ExpressionEnum;

typedef enum {
	EXPRESSION_FLAG_LEAF = 1 << 16,
} ExpressionFlag;

typedef enum {
	EXPRESSION_TYPE_ADD             = EXPRESSION_ENUM_ADD,
	EXPRESSION_TYPE_SUB             = EXPRESSION_ENUM_SUB,
	EXPRESSION_TYPE_MUL             = EXPRESSION_ENUM_MUL,
	EXPRESSION_TYPE_DIV             = EXPRESSION_ENUM_DIV,
	EXPRESSION_TYPE_AND             = EXPRESSION_ENUM_AND,
	EXPRESSION_TYPE_OR              = EXPRESSION_ENUM_OR,
	EXPRESSION_TYPE_XOR             = EXPRESSION_ENUM_XOR,
	EXPRESSION_TYPE_R_DISP          = EXPRESSION_ENUM_R_DISP,
	EXPRESSION_TYPE_L_DISP          = EXPRESSION_ENUM_L_DISP,
	EXPRESSION_TYPE_UNARY_MINUS     = EXPRESSION_ENUM_UNARY_MINUS,
	EXPRESSION_TYPE_UNARY_PLUS      = EXPRESSION_ENUM_UNARY_PLUS,
	EXPRESSION_TYPE_INVERT          = EXPRESSION_ENUM_INVERT,
	EXPRESSION_TYPE_SUBEXPRESSION   = EXPRESSION_ENUM_SUBEXPRESSION,
	EXPRESSION_TYPE_NUMBER          = EXPRESSION_FLAG_LEAF | EXPRESSION_ENUM_NUMBER,
	EXPRESSION_TYPE_DECLARATION     = EXPRESSION_FLAG_LEAF | EXPRESSION_ENUM_DECLARATION,
	EXPRESSION_TYPE_CURRENT_ADDRESS = EXPRESSION_FLAG_LEAF | EXPRESSION_ENUM_CURRENT_ADDRESS,
} ExpressionType;


typedef struct {
	ExpressionType type;
	u32 left_id;
	u32 right_id;
	u32 token_id;
	s64 value;
} Expression;


struct GLOBAL {
	u16  start_addr;
	bool start_addr_overrided;
	u16  entrypoint_addr;
	u16  current_addr;
	char *in_filename;
	char *out_filename;
	bool out_filename_overrided;
	FILE *out_file;
	u8   *out_data;
	u32  out_data_size;
	u32  out_data_cap;

	// CPC SNA
	bool cpc_sna_enabled;
	char *cpc_sna_filename;
	FILE *cpc_sna_file;

	// NOI (the symbol file)
	bool noi_enabled;
	char *noi_filename;
	FILE *noi_file;


	// 
	// All storages have valid values starting from 1
	// we should make a init functions that puts a well defined value in the slots 0
	//
	Token *token_storage;
	u32 token_storage_size;
	u32 token_storage_cap;

	Declaration *declaration_storage;
	u32 declaration_storage_size;
	u32 declaration_storage_cap;

	Placeholder *placeholder_storage;
	u32 placeholder_storage_size;
	u32 placeholder_storage_cap;

	Expression *expression_storage;
	u32 expression_storage_size;
	u32 expression_storage_cap;

	File *file_storage;
	u32 file_storage_size;
	u32 file_storage_cap;

	char **string_storage;
	u32  string_storage_size;
	u32  string_storage_cap;

	TokenIter *token_iter_stack;
	u32 token_iter_stack_cap;
	u32 token_iter_stack_size;

};

struct GLOBAL GLOBAL = {
	.start_addr               = 0,
	.current_addr             = 0,
	.out_filename             = "out.bin",
	.cpc_sna_filename         = "out.sna",
	.noi_filename             = "out.noi",
	.token_storage_size       = 1,
	.declaration_storage_size = 1,
	.placeholder_storage_size = 1,
	.expression_storage_size  = 1,
	.file_storage_size        = 1,
	.string_storage_size      = 1,
};

static void
cleanup() {
	for (u32 i = 1; i < GLOBAL.string_storage_size; ++i) {
		free(GLOBAL.string_storage[i]);
	}
	free(GLOBAL.string_storage);
	free(GLOBAL.declaration_storage);
	free(GLOBAL.token_storage);
	
	free(GLOBAL.out_data);
	if (GLOBAL.out_file != NULL) {
		fclose(GLOBAL.out_file);
	}


	// CPC SNA
	if (GLOBAL.cpc_sna_file != NULL) {
		fclose(GLOBAL.cpc_sna_file);
	}

	// NOI
	if (GLOBAL.noi_file != NULL) {
		fclose(GLOBAL.noi_file);
	}
}

static void
Add_word_to_output(u16);
static void
Add_byte_to_output(u8);
static u32
Save_token(Token *t);


static void
Printerr(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);
}




static char *
File_to_string(const char *filename) {
	FILE *f = fopen(filename, "r");
	if (f == NULL) {
		return NULL;
	}
	fseek(f, 0L, SEEK_END);	
	u64 file_size = ftell(f);
	fseek(f, 0L, SEEK_SET);	
	char *data = NEW(char, file_size + 1);
	if (data == NULL) {
		fclose(f);
		return NULL;
	}
	fread(data, 1, file_size, f);
	fclose(f);

	data[file_size] = '\0';
	return data;
}


static bool
Is_space(char c) {
	return (c == ' ' || c == '\t' || c == '\r');
}

static bool
Is_alpha(char c) {
	return (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z'));
}

static bool
Is_numeric(char c) {
	return ('0' <= c && c <= '9');
}

static bool
String_match(char *match, char *str, u32 str_len) {
	u32 i = 0;
	for(; i < str_len; ++i) {
		if (match[i] == '\0') {
			return false;
		}
		if (match[i] != str[i]) {
			return false;
		}
	}

	return  (match[i] == '\0');
}


static bool
String_to_number_bin(char *str, u32 str_len, s64 *out) {
	s64 result = 0;
	for (u32 i = 0; i < str_len; ++i) {
		if (str[i] == '_') {continue;}
		if (str[i] == '0'){
			result = result * 2;
		} else if (str[i] == '1') {
			result = result * 2 + 1;
		} else {
			return false;
		}
	}
	*out = result;
	return true;
}

static bool
String_to_number_oct(char *str, u32 str_len, s64 *out) {
	s64 result = 0;
	for (u32 i = 0; i < str_len; ++i) {
		if (str[i] == '_') {continue;}
		if (str[i] < '0' || str[i] > '7') {return false;}
		result = result * 8 + (str[i] - '0');
	}
	*out = result;
	return true;
}

static bool
String_to_number_hex(char *str, u32 str_len, s64 *out) {
	s64 result = 0;
	for (u32 i = 0; i < str_len; ++i) {
		if (str[i] == '_') {continue;}

		if      (str[i] == 'a' || str[i] == 'A') {
			result = result * 16 + 10;
		} 
		else if (str[i] == 'b' || str[i] == 'B') {
			result = result * 16 + 11;
		}
		else if (str[i] == 'c' || str[i] == 'C') {
			result = result * 16 + 12;
		}
		else if (str[i] == 'd' || str[i] == 'D') {
			result = result * 16 + 13;
		}
		else if (str[i] == 'e' || str[i] == 'E') {
			result = result * 16 + 14;
		}
		else if (str[i] == 'f' || str[i] == 'F') {
			result = result * 16 + 15;
		}
		else if (str[i] >= '0' && str[i] <= '9') {
			result = result * 16 + (str[i] - '0');
		}
		else {
			return false;
		}
	}
	*out = result;
	return true;
}

static bool
String_to_number_dec(char *str, u32 str_len, s64 *out) {
	s64 result = 0;
	for (u32 i = 0; i < str_len; ++i) {
		if (str[i] == '_') {continue;}
		if (str[i] < '0' || str[i] > '9') {return false;}
		result = result * 10 + (str[i] - '0');
	}
	*out = result;
	return true;
}


static bool
String_to_number(char *str, u32 str_len, s64 *out) {

	bool finding_zero = true;
	for (u32 i = 0; i < str_len; ++i) {
		if (str[i] == '_') {continue;}
		if (finding_zero) {
			if (str[i] == '0') {
				finding_zero = false;
			} else {
				return String_to_number_dec(&str[i], str_len-i, out);
			}
		} else {
			if (str[i] == 'b' || str[i] == 'B') {
				return String_to_number_bin(&str[i+1], str_len-i-1, out);
			}
			if (str[i] == 'o' || str[i] == 'O') {
				return String_to_number_oct(&str[i+1], str_len-i-1, out);
			}
			if (str[i] == 'x' || str[i] == 'X') {
				return String_to_number_hex(&str[i+1], str_len-i-1, out);
			}
			return String_to_number_dec(&str[i], str_len-i, out);
		}
	}
	*out = 0;
	return true;
}


static char *
New_string(u32 size) {
	if (GLOBAL.string_storage_cap <= GLOBAL.string_storage_size) {
		u32  new_cap   = (GLOBAL.string_storage_cap + 1) * 2;
		char **new_data = REALLOC(GLOBAL.string_storage, char*, new_cap);
		if (new_data == NULL) {
			Printerr("Error, cannot allocate enough.\n");
			cleanup();
			exit(-1);
		}
		GLOBAL.string_storage_cap = new_cap;
		GLOBAL.string_storage     = new_data;
	}

	u32 string_id = GLOBAL.string_storage_size;
	GLOBAL.string_storage[string_id] = NEW(char, size);
	if (GLOBAL.string_storage[string_id] == NULL) {
		Printerr("Error, cannot allocate enough.\n");
		cleanup();
		exit(-1);
	}
	GLOBAL.string_storage_size = string_id + 1;
	return GLOBAL.string_storage[string_id];
}




static bool
S64_to_byte(s64 n, u8 *out) {
	if (n < -128 || n > 255) {
		return false;
	}
	*out = (u8)n;
	return true;
}


static bool
S64_to_word(s64 n, u16 *out) {
	if (n < -32768  || n > 65535) {
		return false;
	}
	*out = (u16)n;
	return true;
}


static void
Push_token_iter(TokenIter toki) {
	if (GLOBAL.token_iter_stack_cap <= GLOBAL.token_iter_stack_size) {
		u32  new_cap   = (GLOBAL.token_iter_stack_cap + 1) * 2;
		TokenIter *new_data = REALLOC(GLOBAL.token_iter_stack, TokenIter, new_cap);
		if (new_data == NULL) {
			Printerr("Error, cannot allocate enough.\n");
			cleanup();
			exit(-1);
		}
		GLOBAL.token_iter_stack_cap = new_cap;
		GLOBAL.token_iter_stack     = new_data;
	}

	u32 token_iter_id = GLOBAL.token_iter_stack_size;
	GLOBAL.token_iter_stack[token_iter_id] = toki;
	GLOBAL.token_iter_stack_size = token_iter_id + 1;
}


static TokenIter
Pop_token_iter() {
	assert(GLOBAL.token_iter_stack_size > 0);
	--GLOBAL.token_iter_stack_size;
	return GLOBAL.token_iter_stack[GLOBAL.token_iter_stack_size];
}




static Tokenizer
Make_tokenizer(u32 file_id) {
	Tokenizer result;
	result.file_id           = file_id;
	result.line              = 1;
	result.column            = 1;
	result.stream            = GLOBAL.file_storage[file_id].data_stream;
	result.stream_index      = 0;
	result.stream_line_index = 0;
	result.filename        = GLOBAL.file_storage[file_id].filename;
	return result;
}

static bool
Eat_spaces(Tokenizer *tok) {
	bool result = false;
	for(;;) {
		if(Is_space(tok->stream[tok->stream_index])) {
			++tok->column;
		}
		else {
			return result;
		}
		result = true;
		++tok->stream_index;
	}
}


static void
Eat_multiline_comment(Tokenizer *tok) {
	for (;;) {
		Eat_spaces(tok); // Eat spaces is useful here because handles the \n :)
		if (tok->stream[tok->stream_index] == '\0') {
			return;
		}
		else if (tok->stream[tok->stream_index] == '*' && tok->stream[tok->stream_index+1] == '/') {
			tok->stream_index += 2;
			tok->column += 2;
			return;
		}
		else if (tok->stream[tok->stream_index] == '/' && tok->stream[tok->stream_index+1] == '*') {
			tok->stream_index += 2;
			tok->column += 2;
			Eat_multiline_comment(tok);
			continue;
		}
		else if (tok->stream[tok->stream_index] == '\n') {
			tok->column = 0;
			++tok->line;
			tok->stream_line_index = tok->stream_index + 1;
		}
		++tok->column;
		++tok->stream_index;
	}
}


static void
Eat_line_comment(Tokenizer *tok) {
	for(;;) {
		if (tok->stream[tok->stream_index] == '\0' || tok->stream[tok->stream_index] == '\n'){
			return;
		}
		++tok->stream_index;
	}
}


static bool
Eat_comments(Tokenizer *tok) {
	if (tok->stream[tok->stream_index] == '/' && tok->stream[tok->stream_index+1] == '/') {
		tok->stream_index += 2;
		tok->column += 2;
		Eat_line_comment(tok);
		return true;
	}
	else if (tok->stream[tok->stream_index] == '/' && tok->stream[tok->stream_index+1] == '*') {
		tok->stream_index += 2;
		tok->column += 2;
		Eat_multiline_comment(tok);
		return true;
	}
	return false;
}

static void
Consume_token(Tokenizer *tok, Token *t) {
	
	// Skip all the spaces and comments
	for (;Eat_spaces(tok) || Eat_comments(tok););

	t->file_id    = tok->file_id;
	t->line       = tok->line;
	t->column     = tok->column;
	t->line_index = tok->stream_line_index;
	t->value      = &tok->stream[tok->stream_index];
	t->value_len = 1;
	char c = tok->stream[tok->stream_index];
	switch(c) {
		case ':' : {t->type = TOKEN_TYPE_COLON;             break;}
		case '=' : {t->type = TOKEN_TYPE_ASSIGNMENT;        break;}
		case ';' : {t->type = TOKEN_TYPE_SEMICOLON;         break;}
		case ',' : {t->type = TOKEN_TYPE_COMMA;             break;}
		case '+' : {t->type = TOKEN_TYPE_PLUS;              break;}
		case '*' : {t->type = TOKEN_TYPE_ASTERISK;          break;}
		case '/' : {t->type = TOKEN_TYPE_SLASH;             break;}
		case '(' : {t->type = TOKEN_TYPE_OPEN_PARENTHESIS;  break;}
		case ')' : {t->type = TOKEN_TYPE_CLOSE_PARENTHESIS; break;}
		case '[' : {t->type = TOKEN_TYPE_OPEN_BRACKETS;     break;}
		case ']' : {t->type = TOKEN_TYPE_CLOSE_BRACKETS;    break;}
		case '~' : {t->type = TOKEN_TYPE_TILDE;             break;}
		case '|' : {t->type = TOKEN_TYPE_PIPE;              break;}
		case '&' : {t->type = TOKEN_TYPE_AMPERSAND;         break;}
		case '^' : {t->type = TOKEN_TYPE_CARET;             break;}
		case '$' : {t->type = TOKEN_TYPE_CURRENT_ADDR;      break;}
		case '\0': {
			t->type       = TOKEN_TYPE_EOF;
			t->value_len  = 0;
			return;
		}
		case '\n': {
			t->type       = TOKEN_TYPE_EOL;
			tok->column   = 0;
			++tok->line;
			tok->stream_line_index = tok->stream_index + 1;
			break;
		}
		case '<' : {
			if (tok->stream[tok->stream_index+1] == '<') {
				t->type       = TOKEN_TYPE_R_DISP;
				t->value_len  = 2;
				tok->column  += 1;
				tok->stream_index += 1;
			}
			else {
				t->type = TOKEN_TYPE_UNKNOWN;
			}
			break;
		}
		case '>' : {
			if (tok->stream[tok->stream_index+1] == '>') {
				t->type       = TOKEN_TYPE_L_DISP;
				t->value_len  = 2;
				tok->column  += 1;
				tok->stream_index += 1;
			}
			else {
				t->type = TOKEN_TYPE_UNKNOWN;
			}
			break;
		}
		case '-' : {
			if (tok->stream[tok->stream_index+1] == '-') {
				u32  value_len = 0;
				for (;Is_alpha(c) || Is_numeric(c) || c == '_' || c == '-';) {
					++value_len;
					++tok->column;
					++tok->stream_index;
					c = tok->stream[tok->stream_index];
				}
				if (String_match("--start-addr", t->value, value_len)) {
					t->type = TOKEN_TYPE_FLAG_START_ADDR;
				}
				else if (String_match("--out", t->value, value_len)) {
					t->type = TOKEN_TYPE_FLAG_OUT;
				}
				else if (String_match("--cpc-sna", t->value, value_len)) {
					t->type = TOKEN_TYPE_FLAG_CPC_SNA;
				}
				else if (String_match("--noi", t->value, value_len)) {
					t->type = TOKEN_TYPE_FLAG_NOI;
				}
				else {
					t->type = TOKEN_TYPE_UNKNOWN;
				}
				t->value_len = value_len;
				return;
			}
			else {
				t->type = TOKEN_TYPE_MINUS;
			}
			break;
		}
		case '"' : {
			if (tok->stream[tok->stream_index+1] == '\0') {
				t->type = TOKEN_TYPE_UNKNOWN;
				break;
			}
			++tok->column;       // We skip the starting "
			++tok->stream_index; //

			t->column     = tok->column;
			char *identifier_str = &tok->stream[tok->stream_index];
			u32  identifier_len = 0;
			for (c = tok->stream[tok->stream_index]; c != '"' && c != '\0';) {
				++identifier_len;
				++tok->column;
				++tok->stream_index;
				c = tok->stream[tok->stream_index];
			}
			
			t->type      = TOKEN_TYPE_STRING;
			t->value     = identifier_str;
			t->value_len = identifier_len;
			break;
		}
		case '#' : {
			char *identifier_str = &tok->stream[tok->stream_index];
			u32  identifier_len = 0;
			for (;Is_alpha(c) || Is_numeric(c) || c == '_' || c == '#';) {
				++identifier_len;
				++tok->column;
				++tok->stream_index;
				c = tok->stream[tok->stream_index];
			}
			if      (String_match("#include",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_DIR_INCLUDE;}
			else if (String_match("#compflags", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_DIR_COMPFLAGS;}
			else {t->type = TOKEN_TYPE_UNKNOWN;}
			// t->value     = identifier_str; //This isn't needed we set the start before the switch
			t->value_len = identifier_len;
			return;
		}
		default: {
			if (Is_alpha(c) || c == '_') {
				char *identifier_str = &tok->stream[tok->stream_index];
				u32  identifier_len = 0;
				for (;Is_alpha(c) || Is_numeric(c) || c == '_';) {
					++identifier_len;
					++tok->column;
					++tok->stream_index;
					c = tok->stream[tok->stream_index];
				}
				if      (String_match("af",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_AF;}
				else if (String_match("bc",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_BC;}
				else if (String_match("de",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_DE;}
				else if (String_match("hl",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_HL;}
				else if (String_match("sp",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_SP;}
				else if (String_match("ix",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_IX;}
				else if (String_match("iy",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_IY;}
				else if (String_match("a",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_A;}
				else if (String_match("b",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_B;}
				else if (String_match("c",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_C;}
				else if (String_match("d",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_D;}
				else if (String_match("e",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_E;}
				else if (String_match("h",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_H;}
				else if (String_match("l",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_L;}
				else if (String_match("i",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_I;}
				else if (String_match("r",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_R;}
				else if (String_match("ixh", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_IXH;}
				else if (String_match("ixl", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_IXL;}
				else if (String_match("iyh", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_IYH;}
				else if (String_match("iyl", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_REG_IYL;}

				else if (String_match("byte",    identifier_str, identifier_len)) {t->type = TOKEN_TYPE_KWORD_BYTE;}
				else if (String_match("word",    identifier_str, identifier_len)) {t->type = TOKEN_TYPE_KWORD_WORD;}
				else if (String_match("string",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_KWORD_STRING;}
				else if (String_match("stringz", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_KWORD_STRINGZ;}

				else if (String_match("jp",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_JP  ;}
				else if (String_match("jr",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_JR  ;}
				else if (String_match("djnz", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_DJNZ;}
				else if (String_match("dec",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_DEC ;}
				else if (String_match("inc",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_INC ;}
				else if (String_match("add",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_ADD ;}
				else if (String_match("sub",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_SUB ;}
				else if (String_match("adc",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_ADC ;}
				else if (String_match("sbc",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_SBC ;}
				else if (String_match("or",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_OR  ;}
				else if (String_match("and",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_AND ;}
				else if (String_match("xor",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_XOR ;}
				else if (String_match("cp",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_CP  ;}
				else if (String_match("ld",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_LD  ;}
				else if (String_match("ret",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_RET ;}
				else if (String_match("call", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_CALL;}
				else if (String_match("rst",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_RST ;}
				else if (String_match("out",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_OUT ;}
				else if (String_match("in",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_IN  ;}
				else if (String_match("push", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_PUSH;}
				else if (String_match("pop",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_POP ;}
				else if (String_match("ex",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_EX  ;}
				else if (String_match("im",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_IM  ;}
				else if (String_match("rlc",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_RLC ;}
				else if (String_match("rrc",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_RRC ;}
				else if (String_match("rl",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_RL  ;}
				else if (String_match("rr",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_RR  ;}
				else if (String_match("sla",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_SLA ;}
				else if (String_match("sra",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_SRA ;}
				else if (String_match("sll",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_SLL ;}
				else if (String_match("srl",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_SRL ;}
				else if (String_match("bit",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_BIT ;}
				else if (String_match("res",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_RES ;}
				else if (String_match("set",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_SET ;}
				else if (String_match("nop",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_NOP ;}
				else if (String_match("di",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_DI  ;}
				else if (String_match("ei",   identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_EI  ;}
				else if (String_match("exx",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_EXX ;}
				else if (String_match("rlca", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_RLCA;}
				else if (String_match("rrca", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_RRCA;}
				else if (String_match("rla",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_RLA ;}
				else if (String_match("rra",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_RRA ;}
				else if (String_match("daa",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_DAA ;}
				else if (String_match("cpl",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_CPL ;}
				else if (String_match("scf",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_SCF ;}
				else if (String_match("ccf",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_CCF ;}
				else if (String_match("halt", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_HALT;}
				else if (String_match("retn", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_RETN;}
				else if (String_match("neg",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_NEG ;}
				else if (String_match("outi", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_OUTI;}
				else if (String_match("outd", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_OUTD;}
				else if (String_match("rrd",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_RRD ;}
				else if (String_match("rld",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_RLD ;}
				else if (String_match("ldi",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_LDI ;}
				else if (String_match("cpi",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_CPI ;}
				else if (String_match("ini",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_INI ;}
				else if (String_match("ldd",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_LDD ;}
				else if (String_match("cpd",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_CPD ;}
				else if (String_match("ind",  identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_IND ;}
				else if (String_match("ldir", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_LDIR;}
				else if (String_match("cpir", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_CPIR;}
				else if (String_match("inir", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_INIR;}
				else if (String_match("otir", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_OTIR;}
				else if (String_match("lddr", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_LDDR;}
				else if (String_match("cpdr", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_CPDR;}
				else if (String_match("indr", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_INDR;}
				else if (String_match("otdr", identifier_str, identifier_len)) {t->type = TOKEN_TYPE_INST_OTDR;}

				else {t->type = TOKEN_TYPE_IDENTIFIER;}
				// t->value     = identifier_str; // We set this before the switch
				t->value_len = identifier_len;
				return;
			} else if (Is_numeric(c)){
				u32  number_len = 0;
				for (;Is_alpha(c) || Is_numeric(c) || c == '_';) {
					++number_len;
					++tok->column;
					++tok->stream_index;
					c = tok->stream[tok->stream_index];
				}
				t->type      = TOKEN_TYPE_NUMBER;
				// t->value     = number_str; // We set this before the switch
				t->value_len = number_len;
				return;
			} else {
				t->type = TOKEN_TYPE_UNKNOWN;
			}
			break;
		}
	}


	if (tok->stream[tok->stream_index] != '\0') {
		++tok->column;       // In some cases we may be reached the end before this
		++tok->stream_index; //
	}

	return;
	
}


static TokenIter
Tokenize_file(u32 f_id) {
	assert(f_id != 0);
	Tokenizer tok = Make_tokenizer(f_id);
	Token t;
	TokenIter result;
	Consume_token(&tok, &t);
	result.begin   = GLOBAL.token_storage_size;
	result.current = GLOBAL.token_storage_size;
	for (;t.type != TOKEN_TYPE_EOF; Consume_token(&tok, &t)) {
		Save_token(&t);
	}
	result.end = Save_token(&t);
	return result;
}


static TokenIter *
Prev_token(TokenIter *tok_iter) {
	if (tok_iter->current > tok_iter->begin) {
		--tok_iter->current;
	}
	return tok_iter;
}


static TokenIter *
Next_token(TokenIter *tok_iter) {
	if (tok_iter->current <= tok_iter->end) {
		++tok_iter->current;
	}
	return tok_iter;
}

static Token*
Get_token(TokenIter *tok_iter) {
	return &GLOBAL.token_storage[tok_iter->current];
}


static void
Report_error(Token *t, char *fmt_msg, ...) {
	char *filename    = GLOBAL.file_storage[t->file_id].filename;
	char *file_stream = GLOBAL.file_storage[t->file_id].data_stream;
	fprintf(stderr, ANSI_BWHT "%s:%d:%d: " ANSI_BRED "error: " ANSI_RST,
			filename, t->line, t->column);
    va_list args;
    va_start(args, fmt_msg);
    vfprintf(stderr, fmt_msg, args);
    va_end(args);

	char line_str[50];
	snprintf(line_str, sizeof(line_str),"%d", t->line);
	fprintf(stderr, "\n");
	fprintf(stderr, ANSI_BWHT" %*c |\n"ANSI_RST, (u32)strlen(line_str), ' ');
	fprintf(stderr, ANSI_BWHT" %d | "ANSI_RST, t->line);

	// Prints the current line with the error token in red color
	u32 error_start = t->value - file_stream;
	u32 error_end   = error_start + t->value_len;
	for (u32 i = t->line_index; file_stream[i] != '\0' && file_stream[i] != '\n'; ++i) {
		if    (i == error_start) {fprintf(stderr, ANSI_BRED);}
		else if (i == error_end) {fprintf(stderr, ANSI_RST);}

		// Tabs mess up the output :(
		if (file_stream[i] == '\t') {
			fputc(' ', stderr);
		} else {
			fputc(file_stream[i], stderr);
		}
	}
	
	// Prints a cool arrow pointing at the token
	fprintf(stderr, ANSI_BWHT"\n %*c |"ANSI_RST, (u32)strlen(line_str), ' ');
	fprintf(stderr, "%*c", error_start-t->line_index+1, ' ');
	fprintf(stderr, ANSI_BGRN"^\n"ANSI_RST);
}


#if 0
static void
Print_token(Token *t) {
	switch(t->type) {
		case TOKEN_TYPE_EOF:
			printf("Type: EOF\n");
			break;
		case TOKEN_TYPE_IDENTIFIER:
			printf("Type: IDENTIFIER\n");
			printf ("IS: %.*s\n", t->value_len, t->value);
			break;
		case TOKEN_TYPE_NUMBER:
			printf("Type: NUMBER\n");
			printf ("IS: %.*s\n", t->value_len, t->value);
			break;
		case TOKEN_TYPE_COLON:
			printf("Type: COLON\n");
			break;
		case TOKEN_TYPE_SEMICOLON:
			printf("Type: SEMICOLON\n");
			break;
		case TOKEN_TYPE_COMMA:
			printf("Type: SEMICOLON\n");
			break;
		case TOKEN_TYPE_DQUOTE:
			printf("Type: DQUOTE\n");
			break;
		case TOKEN_TYPE_PLUS:
			printf("Type: PLUS\n");
			break;
		case TOKEN_TYPE_MINUS:
			printf("Type: MINUS\n");
			break;
		default:
			printf("Type: unknow\n");
	}
	printf("Line:   %d\n", t->line);
	printf("Column: %d\n\n", t->column);
}
#endif



static u32
Save_token(Token *t) {
	if (GLOBAL.token_storage_cap <= GLOBAL.token_storage_size) {
		u32  new_cap   = (GLOBAL.token_storage_cap + 1) * 2;
		Token *new_data = REALLOC(GLOBAL.token_storage, Token, new_cap);
		if (new_data == NULL) {
			Printerr("Error, cannot allocate enough.\n");
			cleanup();
			exit(-1);
		}
		GLOBAL.token_storage_cap = new_cap;
		GLOBAL.token_storage     = new_data;
	}

	u32 token_id = GLOBAL.token_storage_size;
	GLOBAL.token_storage[token_id] = *t;
	GLOBAL.token_storage_size = token_id + 1;
	return token_id;
}




static u32
Find_declaration(char *value, u32 len) {
	for (u32 i = 1; i < GLOBAL.declaration_storage_size; ++i) {
		if (len != GLOBAL.declaration_storage[i].value_len) {continue;}
		bool match = true;
		for (u32 j = 0; j < len && match; ++j) {
			if (value[j] != GLOBAL.declaration_storage[i].value[j]) {match = false;}
		}
		if (!match) {continue;}
		return i;
	}
	return 0;
}



static bool
Add_declaration(Declaration *d) {
	u32 already_id = Find_declaration(d->value, d->value_len);
	if (already_id != 0) {
		Token *token_already = &GLOBAL.token_storage[GLOBAL.declaration_storage[already_id].token_id];
		Token *token_to_add  = &GLOBAL.token_storage[d->token_id];
		File  *file_already  = &GLOBAL.file_storage[token_already->file_id];
		Report_error(token_to_add, "%.*s already defined at "ANSI_BWHT"%s:%d:%d"ANSI_RST".", d->value_len, d->value, file_already->filename, token_already->line, token_already->column);
		return false;
	}
	if (GLOBAL.declaration_storage_cap <= GLOBAL.declaration_storage_size) {
		u32  new_cap   = (GLOBAL.declaration_storage_cap + 1) * 2;
		Declaration *new_data = REALLOC(GLOBAL.declaration_storage, Declaration, new_cap);
		if (new_data == NULL) {
			Printerr("Error, cannot allocate enough.\n");
			cleanup();
			exit(-1);
		}
		GLOBAL.declaration_storage_cap = new_cap;
		GLOBAL.declaration_storage     = new_data;
	}
	GLOBAL.declaration_storage[GLOBAL.declaration_storage_size] = *d;
	++GLOBAL.declaration_storage_size;
	return true;
}




static u32
Add_expression(Expression *e) {
	if (GLOBAL.expression_storage_cap <= GLOBAL.expression_storage_size) {
		u32  new_cap   = (GLOBAL.expression_storage_cap + 1) * 2;
		Expression *new_data = REALLOC(GLOBAL.expression_storage, Expression, new_cap);
		if (new_data == NULL) {
			Printerr("Error, cannot allocate enough.\n");
			cleanup();
			exit(-1);
		}
		GLOBAL.expression_storage_cap = new_cap;
		GLOBAL.expression_storage     = new_data;
	}
	u32 expression_id = GLOBAL.expression_storage_size;
	GLOBAL.expression_storage[expression_id] = *e;
	++GLOBAL.expression_storage_size;
	return expression_id;
}




static bool
Add_placeholder(u32 token_id, u32 expr_id, PlaceholderType type) {
	u16 address = GLOBAL.out_data_size;
	if      (type == PLACEHOLDER_TYPE_WORD) {Add_word_to_output(0x0000);}
	else if (type == PLACEHOLDER_TYPE_BYTE) {Add_byte_to_output(0x00);}
	else if (type == PLACEHOLDER_TYPE_REL)  {Add_byte_to_output(0x00);}
	if (GLOBAL.placeholder_storage_cap <= GLOBAL.placeholder_storage_size) {
		u32  new_cap   = (GLOBAL.placeholder_storage_cap + 1) * 2;
		Placeholder *new_data = REALLOC(GLOBAL.placeholder_storage, Placeholder, new_cap);
		if (new_data == NULL) {
			Printerr("Error, cannot allocate enough.\n");
			cleanup();
			exit(-1);
		}
		GLOBAL.placeholder_storage_cap = new_cap;
		GLOBAL.placeholder_storage     = new_data;
	}
	GLOBAL.placeholder_storage[GLOBAL.placeholder_storage_size].token_id = token_id;
	GLOBAL.placeholder_storage[GLOBAL.placeholder_storage_size].expr_id  = expr_id;
	GLOBAL.placeholder_storage[GLOBAL.placeholder_storage_size].type     = type;
	GLOBAL.placeholder_storage[GLOBAL.placeholder_storage_size].address  = address;
	++GLOBAL.placeholder_storage_size;
	return true;
}


typedef enum {
	LOAD_FILE_SUCCES,
	LOAD_FILE_NOT_FOUND,
	LOAD_FILE_CYCLE,
} LoadFileStatus;


static LoadFileStatus
Load_file(char *filename, u32 includer_id, u32 *result) {
	char *absolute_path = New_string(PATH_MAX+1);
	if (realpath(filename, absolute_path) == NULL) {
		return LOAD_FILE_NOT_FOUND;
	}
	
	// We check if the same file is a parent, in that case we have a cycle. That's bad.
	u32 prev_includer = includer_id;
	while(prev_includer != 0) {
		File *prev_file = &GLOBAL.file_storage[prev_includer];
		if (strcmp(prev_file->absolute_path, absolute_path) == 0) {
			return LOAD_FILE_CYCLE;
		}
		prev_includer = prev_file->includer_id;
	}


	char *file_data = File_to_string(filename);
	if (file_data == NULL) {
		return LOAD_FILE_NOT_FOUND;
	}

	if (GLOBAL.file_storage_cap <= GLOBAL.file_storage_size) {
		u32  new_cap   = (GLOBAL.file_storage_cap + 1) * 2;
		File *new_data = (File *)REALLOC(GLOBAL.file_storage, File, new_cap);
		if (new_data == NULL) {
			Printerr("Error, cannot allocate enough.\n");
			cleanup();
			exit(-1);
		}
		GLOBAL.file_storage_cap = new_cap;
		GLOBAL.file_storage     = new_data;
	}
	
	u32 file_id = GLOBAL.file_storage_size;
	GLOBAL.file_storage[file_id].includer_id   = includer_id;
	GLOBAL.file_storage[file_id].filename      = filename;
	GLOBAL.file_storage[file_id].absolute_path = absolute_path;
	GLOBAL.file_storage[file_id].data_stream   = file_data;
	GLOBAL.file_storage_size = file_id + 1;
	*result = file_id;

	return LOAD_FILE_SUCCES;
}




static void
Add_byte_to_output(u8 b) {
	if (GLOBAL.out_data_cap == GLOBAL.out_data_size) {
		u32  new_cap   = (GLOBAL.out_data_cap + 1) * 2;
		u8 *new_data = REALLOC(GLOBAL.out_data, u8, new_cap);
		if (new_data == NULL) {
			Printerr("Error, cannot allocate enough.\n");
			cleanup();
			exit(-1);
		}
		GLOBAL.out_data_cap = new_cap;
		GLOBAL.out_data     = new_data;
	}
	GLOBAL.out_data[GLOBAL.out_data_size] = b;
	++GLOBAL.out_data_size;
}

static void
Add_byte_to_output_at(u8 b, u16 at) {
	GLOBAL.out_data[at] = b;
}


static void
Add_word_to_output(u16 w) {
	u8 h_w = (u8)(w >> 8);
	u8 l_w = (u8)(w & 0x00FF);
	Add_byte_to_output(l_w);
	Add_byte_to_output(h_w);
}


static void
Add_word_to_output_at(u16 w, u16 at) {
	u8 h_w = (u8)(w >> 8);
	u8 l_w = (u8)(w & 0x00FF);
	Add_byte_to_output_at(l_w, at);
	Add_byte_to_output_at(h_w, at+1);
}


//////////////
//
// Adds to the output a byte if the value is less than 256. If not, adds a word.
//
// This is useful when you want to store a instruction that can be extended or not
//
static void
Add_byte_or_word_to_output(u16 bw) {
	if (bw < 256) {
		Add_byte_to_output((u8)bw);
	} else {
		Add_word_to_output(bw);
	}
}


static bool
Token_to_byte_or_report(Token *t, u8 *n) {
	s64 number;
	if (!String_to_number(t->value, t->value_len, &number)) {
		Report_error(t, "Wrong formated number.");
		return false;
	}
	if (!S64_to_byte(number, n)) {
		Report_error(t, "Value %lu overflows inside a byte.", number);
		return false;
	}
	return true;
}

static bool
Token_to_word_or_report(Token *t, u16 *n) {
	s64 number;
	if (!String_to_number(t->value, t->value_len, &number)) {
		Report_error(t, "Wrong formated number.");
		return false;
	}
	if (!S64_to_word(number, n)) {
		Report_error(t, "Value %lu overflows inside a word.", number);
		return false;
	}
	return true;
}


static bool
Token_is_or_report(Token *t, TokenType type, char *identifier_match) {
	if (type == TOKEN_TYPE_IDENTIFIER && identifier_match != NULL)  {
		if (t->type == type && String_match(identifier_match, t->value, t->value_len)) {
			return true;
		}
		Report_error(t, "Expected identifier "ANSI_BWHT"'%s'"ANSI_RST".", identifier_match);
		return false;
	}
	if (t->type == type) {
		return true;
	}
	Report_error(t, "Expected "ANSI_BWHT"'%s'"ANSI_RST".", token_type_strings[0x0000FFFF & type]);
	return false;
}


static bool
Token_is(Token *t, TokenType type, char *identifier_match) {
	if (t->type != type) {
		return false;
	}
	if ((type == TOKEN_TYPE_IDENTIFIER || type == TOKEN_TYPE_NUMBER) && identifier_match != NULL)  {
		return String_match(identifier_match, t->value, t->value_len);
	}
	return true;
}


static bool
Expect_end_of_statement(Token *t) {
	if (t->type & TOKEN_FLAG_END_OF_STATEMENT) {
		return true;
	}
	Report_error(t, "Expected end of statement with: "ANSI_BWHT"'\\n'"ANSI_RST" or "ANSI_BWHT"';'"ANSI_RST".");
	return false;
}

static bool
Expect(TokenIter *tok_iter, TokenType type, char *identifier_match) {
	return Token_is(&GLOBAL.token_storage[tok_iter->current], type, identifier_match);
}

static bool
Expect_or_report(TokenIter *tok_iter, TokenType type, char *identifier_match) {
	return Token_is_or_report(&GLOBAL.token_storage[tok_iter->current], type, identifier_match);
}









//////////////////////////////////////////////////////////////////////////////////////////////
//
//
//
//  EXPRESSIONS CODE
//
//
//
/////////////////////////////////////////////////////////////////////////////////////////////


static u32
Get_precedence(TokenType type) {
	switch (type) {
		case TOKEN_TYPE_ASTERISK:
		case TOKEN_TYPE_SLASH:
		case TOKEN_TYPE_AMPERSAND:
		case TOKEN_TYPE_PIPE:
		case TOKEN_TYPE_CARET:
		case TOKEN_TYPE_R_DISP:
		case TOKEN_TYPE_L_DISP:
			return 10;
		default:
			return 0;
	}
}


static u32
Parse_expression_internal(TokenIter *tok_iter, u32 parent_id, u32 precedence, bool *p_trigger, bool from_unary) {
	u32 left_token_id = tok_iter->current;
	Token *left_token = Get_token(tok_iter);
	u32 left_e        = 0;

	if (!(left_token->type & TOKEN_FLAG_EXPR)) {
		return 0;
	}

	if (left_token->type == TOKEN_TYPE_CLOSE_PARENTHESIS) {return 0;}
	
	if (left_token->type == TOKEN_TYPE_NUMBER ||
		left_token->type == TOKEN_TYPE_IDENTIFIER ||
		left_token->type == TOKEN_TYPE_CURRENT_ADDR){

		Expression expr;
		if      (left_token->type == TOKEN_TYPE_NUMBER) {expr.type = EXPRESSION_TYPE_NUMBER;}
		else if (left_token->type == TOKEN_TYPE_IDENTIFIER) {expr.type = EXPRESSION_TYPE_DECLARATION;}
		else if (left_token->type == TOKEN_TYPE_CURRENT_ADDR) {
			expr.type = EXPRESSION_TYPE_CURRENT_ADDRESS;
			expr.value = GLOBAL.current_addr;
		}
		expr.token_id = left_token_id;
		expr.left_id  = 0;
		expr.right_id = 0;
		left_e = Add_expression(&expr);
		if (from_unary) {
			return left_e;
		}
	}
	else if (left_token->type & TOKEN_FLAG_OPER_UNARY) {
		//     
		//  -2 + 1
		//  
		//       -        +
		//      /        / \
		//     +        -   1
		//    / \      /
		//   2   1    2
		//
		//  :(((((
		//
		Expression expr;
		if      (left_token->type == TOKEN_TYPE_PLUS)  {expr.type = EXPRESSION_TYPE_UNARY_PLUS;}
		else if (left_token->type == TOKEN_TYPE_MINUS) {expr.type = EXPRESSION_TYPE_UNARY_MINUS;}
		else if (left_token->type == TOKEN_TYPE_TILDE) {expr.type = EXPRESSION_TYPE_INVERT;}
		else    {assert(false);}
		expr.token_id = left_token_id;
		expr.right_id = 0;
		expr.left_id = Parse_expression_internal(Next_token(tok_iter), 0, 0, NULL, /*From unary*/ true);
		if (expr.left_id == 0) {return 0;}
		left_e = Add_expression(&expr);
		if (from_unary) {
			return left_e;
		}
	}
	else if (left_token->type == TOKEN_TYPE_OPEN_PARENTHESIS) {
		Expression expr;
		expr.type = EXPRESSION_TYPE_SUBEXPRESSION;
		expr.token_id = left_token_id;
		expr.left_id  = Parse_expression_internal(Next_token(tok_iter), 0, 0, NULL, /*From unary*/ false);
		expr.right_id = 0;
		if (expr.left_id == 0) {return 0;}
		left_e = Add_expression(&expr);
		if (from_unary) {
			return left_e;
		}

		// If now the token isn't close parenthesis something went wrong...
		if (Get_token(tok_iter)->type != TOKEN_TYPE_CLOSE_PARENTHESIS) {return 0;}
	}
	else {
		return 0;
	}

	Next_token(tok_iter);
	u32 operator_token_id = tok_iter->current;
	Token *operator_token = Get_token(tok_iter);
	Expression operator_expr;
	if      (Token_is(operator_token, TOKEN_TYPE_PLUS, NULL))      {operator_expr.type = EXPRESSION_TYPE_ADD;}
	else if (Token_is(operator_token, TOKEN_TYPE_MINUS, NULL))     {operator_expr.type = EXPRESSION_TYPE_SUB;}
	else if (Token_is(operator_token, TOKEN_TYPE_ASTERISK, NULL))  {operator_expr.type = EXPRESSION_TYPE_MUL;}
	else if (Token_is(operator_token, TOKEN_TYPE_SLASH, NULL))     {operator_expr.type = EXPRESSION_TYPE_DIV;}
	else if (Token_is(operator_token, TOKEN_TYPE_PIPE, NULL))      {operator_expr.type = EXPRESSION_TYPE_OR;}
	else if (Token_is(operator_token, TOKEN_TYPE_AMPERSAND, NULL)) {operator_expr.type = EXPRESSION_TYPE_AND;}
	else if (Token_is(operator_token, TOKEN_TYPE_CARET, NULL))     {operator_expr.type = EXPRESSION_TYPE_XOR;}
	else if (Token_is(operator_token, TOKEN_TYPE_R_DISP, NULL))    {operator_expr.type = EXPRESSION_TYPE_R_DISP;}
	else if (Token_is(operator_token, TOKEN_TYPE_L_DISP, NULL))    {operator_expr.type = EXPRESSION_TYPE_L_DISP;}
	else {return left_e;}
	u32 local_precedence = Get_precedence(operator_token->type);
	operator_expr.token_id = operator_token_id;
	

	// For example 2*3+4
	//
	//       *(Precedence = 10)               +
	//      / \                   ->         / \
	//     2   +(Precedence = 0)  TO        *   4
	//        / \                 ->       / \
	//       3   4                        2   3
	//
	// We fix the precendence in this if else
	if (parent_id != 0 && precedence >= local_precedence) {
		assert(parent_id != 0);
		GLOBAL.expression_storage[parent_id].right_id = left_e;
		*p_trigger = true;
		operator_expr.left_id  = parent_id;
	} else {
		operator_expr.left_id  = left_e;
	}

	u32 operator_expr_id = Add_expression(&operator_expr);
	bool p_trigger_local = false;
	u32 right_expression = Parse_expression_internal(Next_token(tok_iter), operator_expr_id, local_precedence, &p_trigger_local, /* From unary */ false);

	// If the precedence was triggered, the id to return will be the right_expression
	if (p_trigger_local) {
		return right_expression;
	} else if (right_expression == 0) {
		return 0;
	} else {
		GLOBAL.expression_storage[operator_expr_id].right_id = right_expression;
		return operator_expr_id;
	}
}



static u32
Parse_expression(TokenIter *tok_iter) {
	return Parse_expression_internal(
			tok_iter,
			0,    // parent_id
			0,    // precedence
			NULL, // p_trigger
			false // from_unary
	);
}



static bool
Compute_expression(u32 e_id, s64 *result) {
	assert(e_id != 0 && e_id < GLOBAL.expression_storage_size);
	Expression *e = &GLOBAL.expression_storage[e_id];
	s64 l_result, r_result;
	if (e->type & EXPRESSION_FLAG_LEAF) {
		Token *leaf_token = &GLOBAL.token_storage[e->token_id];
		if (e->type == EXPRESSION_TYPE_NUMBER) {
			return String_to_number(leaf_token->value, leaf_token->value_len, result);
		}
		else if (e->type == EXPRESSION_TYPE_DECLARATION) {
			u32 decl_id = Find_declaration(leaf_token->value, leaf_token->value_len);
			if (decl_id == 0) {
				Report_error(leaf_token, "Identifier, don't declared.");
				return false;
			}
			Declaration *decl = &GLOBAL.declaration_storage[decl_id];
			if (!decl->resolved) {
				Report_error(leaf_token, "Trying to compute unresolved declaration.");
				return false;
			}
			*result = decl->result;
			return true;
		}
		else if (e->type == EXPRESSION_TYPE_CURRENT_ADDRESS) {
			*result = e->value + GLOBAL.start_addr;
			return true;
		}
		else {
			assert(0);
		}
	}
	else {
		if (!Compute_expression(e->left_id, &l_result))  {return false;}
		
		if      (e->type == EXPRESSION_TYPE_UNARY_PLUS)    {*result =  l_result;}
		else if (e->type == EXPRESSION_TYPE_UNARY_MINUS)   {*result = -l_result;}
		else if (e->type == EXPRESSION_TYPE_INVERT)        {*result = ~l_result;}
		else if (e->type == EXPRESSION_TYPE_SUBEXPRESSION) {*result =  l_result;}
		else {
			if (!Compute_expression(e->right_id, &r_result)) {return false;}

			if      (e->type == EXPRESSION_TYPE_ADD) {*result = l_result + r_result;}
			else if (e->type == EXPRESSION_TYPE_SUB) {*result = l_result - r_result;}
			else if (e->type == EXPRESSION_TYPE_MUL) {*result = l_result * r_result;}
			else if (e->type == EXPRESSION_TYPE_DIV) {*result = l_result / r_result;}
			else if (e->type == EXPRESSION_TYPE_OR)  {*result = l_result | r_result;}
			else if (e->type == EXPRESSION_TYPE_AND) {*result = l_result & r_result;}
			else if (e->type == EXPRESSION_TYPE_XOR) {*result = l_result ^ r_result;}
			else if (e->type == EXPRESSION_TYPE_R_DISP) {*result = l_result << r_result;}
			else if (e->type == EXPRESSION_TYPE_L_DISP) {*result = l_result >> r_result;}
			else    {return false;}
		}
		return true;
	}
	return true;
}



static bool
Compute_expression_or_report(u32 e_id, s64 *result) {
	if (!Compute_expression(e_id, result)) {
		Expression *e = &GLOBAL.expression_storage[e_id];
		Token *t = &GLOBAL.token_storage[e->token_id];
		Report_error(t, "Cannot compute expression.");
		return false;
	}
	return true;
}












/////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//                       HERE STARTS THE KEYWORD HANDLING HELL
//
//
//
//
//
//  | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | |
//  V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V



static bool
Parse_byte_or_word(TokenIter *tok, bool is_byte) {
	u32 values_specified = 0;
	const u8 FINDING_EXPRESSION       = 1 << 0;
	const u8 FINDING_COMMA            = 1 << 1;
	const u8 FINDING_END_OF_STATEMENT = 1 << 2;
	u8 finding = FINDING_EXPRESSION | FINDING_END_OF_STATEMENT;
	Next_token(tok);
	for (;;) {
		Token *t = Get_token(tok);
		u32 token_id = tok->current;
		if ((finding & FINDING_EXPRESSION) && (t->type & TOKEN_FLAG_EXPR)) {
			u32 expression_id = Parse_expression(tok);
			if (expression_id == 0) {
				Report_error(Get_token(tok), "Cannot parse expression.");
				return false;
			}
			if (is_byte) {
				Add_placeholder(token_id, expression_id, PLACEHOLDER_TYPE_BYTE);
			} else {
				Add_placeholder(token_id, expression_id, PLACEHOLDER_TYPE_WORD);
			}
			++values_specified;
			finding = FINDING_COMMA | FINDING_END_OF_STATEMENT;
			continue;
		}
		else if ((finding & FINDING_COMMA) && Expect(tok, TOKEN_TYPE_COMMA, NULL)) {
			finding = FINDING_EXPRESSION;
		}
		else if ((finding & FINDING_END_OF_STATEMENT) && (Get_token(tok)->type & TOKEN_FLAG_END_OF_STATEMENT)) {
			if (values_specified == 0) {
				if (is_byte) {
					Add_byte_to_output(0);
				} else {
					Add_word_to_output(0);
				}
			}
			return true;

		}
		else {
			Report_error(Get_token(tok), "Unexpected token.");
			return false;
		}
		Next_token(tok);
	}
}


static bool
Parse_string_or_stringz(TokenIter *tok_iter, bool is_string) {
	Next_token(tok_iter);
	Token *string_token = Get_token(tok_iter);
	if (!Token_is_or_report(string_token, TOKEN_TYPE_STRING, NULL)) {return false;}
	for (u32 i = 0; i < string_token->value_len; ++i) {
		Add_byte_to_output(string_token->value[i]);
	}
	if (!is_string) {
		Add_byte_to_output(0);
	}
	Next_token(tok_iter);
	return Expect_end_of_statement(Get_token(tok_iter));
}


static bool
Parse_inc_or_dec(TokenIter *tok, bool is_inc) {
	Next_token(tok);
	if      (Expect(tok, TOKEN_TYPE_REG_BC,  NULL)) {Add_byte_to_output(is_inc ? 0x03 : 0x0B);}
	else if (Expect(tok, TOKEN_TYPE_REG_DE,  NULL)) {Add_byte_to_output(is_inc ? 0x13 : 0x1B);}
	else if (Expect(tok, TOKEN_TYPE_REG_HL,  NULL)) {Add_byte_to_output(is_inc ? 0x23 : 0x2B);}
	else if (Expect(tok, TOKEN_TYPE_REG_SP,  NULL)) {Add_byte_to_output(is_inc ? 0x33 : 0x3B);}
	else if (Expect(tok, TOKEN_TYPE_REG_B,   NULL)) {Add_byte_to_output(is_inc ? 0x04 : 0x05);}
	else if (Expect(tok, TOKEN_TYPE_REG_D,   NULL)) {Add_byte_to_output(is_inc ? 0x14 : 0x15);}
	else if (Expect(tok, TOKEN_TYPE_REG_H,   NULL)) {Add_byte_to_output(is_inc ? 0x24 : 0x25);}
	else if (Expect(tok, TOKEN_TYPE_REG_C,   NULL)) {Add_byte_to_output(is_inc ? 0x0C : 0x0D);}
	else if (Expect(tok, TOKEN_TYPE_REG_E,   NULL)) {Add_byte_to_output(is_inc ? 0x1C : 0x1D);}
	else if (Expect(tok, TOKEN_TYPE_REG_L,   NULL)) {Add_byte_to_output(is_inc ? 0x2C : 0x2D);}
	else if (Expect(tok, TOKEN_TYPE_REG_A,   NULL)) {Add_byte_to_output(is_inc ? 0x3C : 0x3D);}
	else if (Expect(tok, TOKEN_TYPE_REG_IX,  NULL)) {Add_word_to_output(is_inc ? 0x23DD : 0x2BDD);}
	else if (Expect(tok, TOKEN_TYPE_REG_IXH, NULL)) {Add_word_to_output(is_inc ? 0x24DD : 0x25DD);}
	else if (Expect(tok, TOKEN_TYPE_REG_IXL, NULL)) {Add_word_to_output(is_inc ? 0x2CDD : 0x2DDD);}
	else if (Expect(tok, TOKEN_TYPE_REG_IY,  NULL)) {Add_word_to_output(is_inc ? 0x23FD : 0x2BFD);}
	else if (Expect(tok, TOKEN_TYPE_REG_IYH, NULL)) {Add_word_to_output(is_inc ? 0x24FD : 0x25FD);}
	else if (Expect(tok, TOKEN_TYPE_REG_IYL, NULL)) {Add_word_to_output(is_inc ? 0x2CFD : 0x2DFD);}
	else if (Expect(tok, TOKEN_TYPE_OPEN_BRACKETS, NULL)) {
		Next_token(tok);
		if (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {Add_byte_to_output(is_inc ? 0x34 : 0x35); Next_token(tok);}
		else {
			if      (Expect(tok, TOKEN_TYPE_REG_IX, NULL)) {Add_word_to_output(is_inc ? 0x34DD : 0x35DD);}
			else if (Expect(tok, TOKEN_TYPE_REG_IY, NULL)) {Add_word_to_output(is_inc ? 0x34FD : 0x35FD);}
			else {
				Report_error(Get_token(tok), "Expected a register [hl], [ix+*] or [iy+*].");
				return false;
			}
			u8 offset = 0;
			Next_token(tok);
			Token *curr_tok = Get_token(tok);
			if (curr_tok->type == TOKEN_TYPE_PLUS || curr_tok->type == TOKEN_TYPE_MINUS) {
				u32 token_id = tok->current;
				u32 expr_id = Parse_expression(tok);
				if (expr_id == 0) {
					Report_error(Get_token(tok), "Cannot parse expression.");
					return false;
				}
				Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_BYTE);
			}
			else {
				Add_byte_to_output(offset);
			}
		}
		if (!Expect_or_report(tok, TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
	}
	else {
		Report_error(Get_token(tok), "Expected a valid register.");
	}
	
	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}








static bool
HANDLE_misc_instructions(TokenIter *tok) {
	//
	// @speed
	// This function is checking for the identifiers aganin. Maybe we should find faster way.
	// But this solution seems clean to me.
	//
	if      (Expect(tok, TOKEN_TYPE_INST_NOP, NULL))  {Add_byte_to_output(0x00);}
	else if (Expect(tok, TOKEN_TYPE_INST_DI, NULL))   {Add_byte_to_output(0xF3);}
	else if (Expect(tok, TOKEN_TYPE_INST_EI, NULL))   {Add_byte_to_output(0xFB);}
	else if (Expect(tok, TOKEN_TYPE_INST_EXX, NULL))  {Add_byte_to_output(0xD9);}
	else if (Expect(tok, TOKEN_TYPE_INST_RLCA, NULL)) {Add_byte_to_output(0x07);}
	else if (Expect(tok, TOKEN_TYPE_INST_RRCA, NULL)) {Add_byte_to_output(0x0F);}
	else if (Expect(tok, TOKEN_TYPE_INST_RLA, NULL))  {Add_byte_to_output(0x17);}
	else if (Expect(tok, TOKEN_TYPE_INST_RRA, NULL))  {Add_byte_to_output(0x1F);}
	else if (Expect(tok, TOKEN_TYPE_INST_DAA, NULL))  {Add_byte_to_output(0x27);}
	else if (Expect(tok, TOKEN_TYPE_INST_CPL, NULL))  {Add_byte_to_output(0x2F);}
	else if (Expect(tok, TOKEN_TYPE_INST_SCF, NULL))  {Add_byte_to_output(0x37);}
	else if (Expect(tok, TOKEN_TYPE_INST_CCF, NULL))  {Add_byte_to_output(0x3F);}
	else if (Expect(tok, TOKEN_TYPE_INST_HALT, NULL)) {Add_byte_to_output(0x76);}
	else if (Expect(tok, TOKEN_TYPE_INST_RETN, NULL)) {Add_word_to_output(0x45ED);}
	else if (Expect(tok, TOKEN_TYPE_INST_NEG, NULL))  {Add_word_to_output(0x44ED);}
	else if (Expect(tok, TOKEN_TYPE_INST_OUTI, NULL)) {Add_word_to_output(0xA3ED);}
	else if (Expect(tok, TOKEN_TYPE_INST_OUTD, NULL)) {Add_word_to_output(0xABED);}
	else if (Expect(tok, TOKEN_TYPE_INST_RRD, NULL))  {Add_word_to_output(0x67ED);}
	else if (Expect(tok, TOKEN_TYPE_INST_RLD, NULL))  {Add_word_to_output(0x6FED);}
	else if (Expect(tok, TOKEN_TYPE_INST_LDI, NULL))  {Add_word_to_output(0xA0ED);}
	else if (Expect(tok, TOKEN_TYPE_INST_CPI, NULL))  {Add_word_to_output(0xA1ED);}
	else if (Expect(tok, TOKEN_TYPE_INST_INI, NULL))  {Add_word_to_output(0xA2ED);}
	else if (Expect(tok, TOKEN_TYPE_INST_LDD, NULL))  {Add_word_to_output(0xA8ED);}
	else if (Expect(tok, TOKEN_TYPE_INST_CPD, NULL))  {Add_word_to_output(0xA9ED);}
	else if (Expect(tok, TOKEN_TYPE_INST_IND, NULL))  {Add_word_to_output(0xAAED);}
	else if (Expect(tok, TOKEN_TYPE_INST_LDIR, NULL)) {Add_word_to_output(0xB0ED);}
	else if (Expect(tok, TOKEN_TYPE_INST_CPIR, NULL)) {Add_word_to_output(0xB1ED);}
	else if (Expect(tok, TOKEN_TYPE_INST_INIR, NULL)) {Add_word_to_output(0xB2ED);}
	else if (Expect(tok, TOKEN_TYPE_INST_OTIR, NULL)) {Add_word_to_output(0xB3ED);}
	else if (Expect(tok, TOKEN_TYPE_INST_LDDR, NULL)) {Add_word_to_output(0xB8ED);}
	else if (Expect(tok, TOKEN_TYPE_INST_CPDR, NULL)) {Add_word_to_output(0xB9ED);}
	else if (Expect(tok, TOKEN_TYPE_INST_INDR, NULL)) {Add_word_to_output(0xBAED);}
	else if (Expect(tok, TOKEN_TYPE_INST_OTDR, NULL)) {Add_word_to_output(0xBBED);}
	else {assert(false);} // This must be checket before
	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}


static bool
HANDLE_push_or_pop(TokenIter *tok, bool is_push) {
	Next_token(tok);
	if      (Expect(tok, TOKEN_TYPE_REG_BC, NULL)) {Add_byte_to_output(is_push ? 0xC5 : 0xC1);}
	else if (Expect(tok, TOKEN_TYPE_REG_DE, NULL)) {Add_byte_to_output(is_push ? 0xD5 : 0xD1);}
	else if (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {Add_byte_to_output(is_push ? 0xE5 : 0xE1);}
	else if (Expect(tok, TOKEN_TYPE_REG_AF, NULL)) {Add_byte_to_output(is_push ? 0xF5 : 0xF1);}
	else if (Expect(tok, TOKEN_TYPE_REG_IX, NULL)) {Add_word_to_output(is_push ? 0xE5DD : 0xE1DD);}
	else if (Expect(tok, TOKEN_TYPE_REG_IY, NULL)) {Add_word_to_output(is_push ? 0xE5FD : 0xE1FD);}
	else {
		Report_error(Get_token(tok), "Expected 'bc', 'de', 'hl', 'af', 'ix' or 'iy'.");
		return false;
	}
	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}


static bool
HANDLE_push(TokenIter *tok) {
	return HANDLE_push_or_pop(tok, true);
}





static bool
HANDLE_pop(TokenIter *tok) {
	return HANDLE_push_or_pop(tok, false);
}







static bool
HANDLE_bit_res_set(TokenIter *tok, u32 op) {
	Next_token(tok);
	if (!Expect_or_report(tok, TOKEN_TYPE_NUMBER, NULL)) {return false;}
	s64 bit_id;
	if (!String_to_number(Get_token(tok)->value, Get_token(tok)->value_len, &bit_id)) {Report_error(Get_token(tok), "Wrong format number."); return false;}
	if (bit_id < 0 || bit_id > 7) {Report_error(Get_token(tok), "Expected number between '0' and '7'."); return false;}
	if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA, NULL)) {return false;}
	Next_token(tok);
	if (Expect(tok, TOKEN_TYPE_REG_B, NULL)) {
		u16 w = (u16[3][8]){
			{0x40CB,0x48CB,0x50CB,0x58CB,0x60CB,0x68CB,0x70CB,0x78CB},
			{0x80CB,0x88CB,0x90CB,0x98CB,0xA0CB,0xA8CB,0xB0CB,0xB8CB},
			{0xC0CB,0xC8CB,0xD0CB,0xD8CB,0xE0CB,0xE8CB,0xF0CB,0xF8CB}}[op][bit_id];
		Add_word_to_output(w);
	}
	else if (Expect(tok, TOKEN_TYPE_REG_C, NULL)) {
		u16 w = (u16[3][8]){
			{0x41CB,0x49CB,0x51CB,0x59CB,0x61CB,0x69CB,0x71CB,0x79CB},
			{0x81CB,0x89CB,0x91CB,0x99CB,0xA1CB,0xA9CB,0xB1CB,0xB9CB},
			{0xC1CB,0xC9CB,0xD1CB,0xD9CB,0xE1CB,0xE9CB,0xF1CB,0xF9CB}}[op][bit_id];
		Add_word_to_output(w);
	}
	else if (Expect(tok, TOKEN_TYPE_REG_D, NULL)) {
		u16 w = (u16[3][8]){
			{0x42CB,0x4ACB,0x52CB,0x5ACB,0x62CB,0x6ACB,0x72CB,0x7ACB},
			{0x82CB,0x8ACB,0x92CB,0x9ACB,0xA2CB,0xAACB,0xB2CB,0xBACB},
			{0xC2CB,0xCACB,0xD2CB,0xDACB,0xE2CB,0xEACB,0xF2CB,0xFACB}}[op][bit_id];
		Add_word_to_output(w);
	}
	else if (Expect(tok, TOKEN_TYPE_REG_E, NULL)) {
		u16 w = (u16[3][8]){
			{0x43CB,0x4BCB,0x53CB,0x5BCB,0x63CB,0x6BCB,0x73CB,0x7BCB},
			{0x83CB,0x8BCB,0x93CB,0x9BCB,0xA3CB,0xABCB,0xB3CB,0xBBCB},
			{0xC3CB,0xCBCB,0xD3CB,0xDBCB,0xE3CB,0xEBCB,0xF3CB,0xFBCB}}[op][bit_id];
		Add_word_to_output(w);
	}
	else if (Expect(tok, TOKEN_TYPE_REG_H, NULL)) {
		u16 w = (u16[3][8]){
			{0x44CB,0x4CCB,0x54CB,0x5CCB,0x64CB,0x6CCB,0x74CB,0x7CCB},
			{0x84CB,0x8CCB,0x94CB,0x9CCB,0xA4CB,0xACCB,0xB4CB,0xBCCB},
			{0xC4CB,0xCCCB,0xD4CB,0xDCCB,0xE4CB,0xECCB,0xF4CB,0xFCCB}}[op][bit_id];
		Add_word_to_output(w);
	}
	else if (Expect(tok, TOKEN_TYPE_REG_L, NULL)) {
		u16 w = (u16[3][8]){
			{0x45CB,0x4DCB,0x55CB,0x5DCB,0x65CB,0x6DCB,0x75CB,0x7DCB},
			{0x85CB,0x8DCB,0x95CB,0x9DCB,0xA5CB,0xADCB,0xB5CB,0xBDCB},
			{0xC5CB,0xCDCB,0xD5CB,0xDDCB,0xE5CB,0xEDCB,0xF5CB,0xFDCB}}[op][bit_id];
		Add_word_to_output(w);
	}
	else if (Expect(tok, TOKEN_TYPE_REG_A, NULL)) {
		u16 w = (u16[3][8]){
			{0x47CB,0x4FCB,0x57CB,0x5FCB,0x67CB,0x6FCB,0x77CB,0x7FCB},
			{0x87CB,0x8FCB,0x97CB,0x9FCB,0xA7CB,0xAFCB,0xB7CB,0xBFCB},
			{0xC7CB,0xCFCB,0xD7CB,0xDFCB,0xE7CB,0xEFCB,0xF7CB,0xFFCB}}[op][bit_id];
		Add_word_to_output(w);
	}
	else if (Expect(tok, TOKEN_TYPE_OPEN_BRACKETS, NULL)) {
		bool is_hl = false;
		Next_token(tok);
		if (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {
			u16 w = (u16[3][8]){
				{0x46CB,0x4ECB,0x56CB,0x5ECB,0x66CB,0x6ECB,0x76CB,0x7ECB},
				{0x86CB,0x8ECB,0x96CB,0x9ECB,0xA6CB,0xAECB,0xB6CB,0xBECB},
				{0xC6CB,0xCECB,0xD6CB,0xDECB,0xE6CB,0xEECB,0xF6CB,0xFECB}}[op][bit_id];
			Add_word_to_output(w);
			is_hl=true;
		}
		else if (Expect(tok, TOKEN_TYPE_REG_IX, NULL)) {Add_word_to_output(0xCBDD);}
		else if (Expect(tok, TOKEN_TYPE_REG_IY, NULL)) {Add_word_to_output(0xCBFD);}
		else {
			Report_error(Get_token(tok), "Expected '[hl]', '[ix+*]' or '[iy+*]'.");
			return false;
		}
		Next_token(tok);
		if (is_hl) {
			if (!Expect_or_report(tok, TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
			Next_token(tok);
			return Expect_end_of_statement(Get_token(tok));
		}

		Token *curr_tok = Get_token(tok);
		if (curr_tok->type == TOKEN_TYPE_PLUS || curr_tok->type == TOKEN_TYPE_MINUS) {
			u32 token_id = tok->current;
			u32 expr_id  = Parse_expression(tok);
			if (expr_id == 0) {
				Report_error(Get_token(tok), "Cannot parse expression.");
				return false;
			}
			Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_BYTE);
		}
		else {
			Add_byte_to_output(0);
		}

		if (!Expect_or_report(tok, TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
		
		Next_token(tok);
		if (Get_token(tok)->type == TOKEN_TYPE_COMMA && op > 0) {
			Next_token(tok);
			if (Get_token(tok)->type == TOKEN_TYPE_REG_B) {
				u8 b = (u8[3][8]){
					{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},
					{0x80,0x88,0x90,0x98,0xA0,0xA8,0xB0,0xB8},
					{0xC0,0xC8,0xD0,0xD8,0xE0,0xE8,0xF0,0xF8}}[op][bit_id];
				Add_byte_to_output(b);
			}
			else if (Get_token(tok)->type == TOKEN_TYPE_REG_C) {
				u8 b = (u8[3][8]){
					{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},
					{0x81,0x89,0x91,0x99,0xA1,0xA9,0xB1,0xB9},
					{0xC1,0xC9,0xD1,0xD9,0xE1,0xE9,0xF1,0xF9}}[op][bit_id];
				Add_byte_to_output(b);
			}
			else if (Get_token(tok)->type == TOKEN_TYPE_REG_D) {
				u8 b = (u8[3][8]){
					{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},
					{0x82,0x8A,0x92,0x9A,0xA2,0xAA,0xB2,0xBA},
					{0xC2,0xCA,0xD2,0xDA,0xE2,0xEA,0xF2,0xFA}}[op][bit_id];
				Add_byte_to_output(b);
			}
			else if (Get_token(tok)->type == TOKEN_TYPE_REG_E) {
				u8 b = (u8[3][8]){
					{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},
					{0x83,0x8B,0x93,0x9B,0xA3,0xAB,0xB3,0xBB},
					{0xC3,0xCB,0xD3,0xDB,0xE3,0xEB,0xF3,0xFB}}[op][bit_id];
				Add_byte_to_output(b);
			}
			else if (Get_token(tok)->type == TOKEN_TYPE_REG_H) {
				u8 b = (u8[3][8]){
					{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},
					{0x84,0x8C,0x94,0x9C,0xA4,0xAC,0xB4,0xBC},
					{0xC4,0xCC,0xD4,0xDC,0xE4,0xEC,0xF4,0xFC}}[op][bit_id];
				Add_byte_to_output(b);
			}
			else if (Get_token(tok)->type == TOKEN_TYPE_REG_L) {
				u8 b = (u8[3][8]){
					{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},
					{0x85,0x8D,0x95,0x9D,0xA5,0xAD,0xB5,0xBD},
					{0xC5,0xCD,0xD5,0xDD,0xE5,0xED,0xF5,0xFD}}[op][bit_id];
				Add_byte_to_output(b);
			}
			else if (Get_token(tok)->type == TOKEN_TYPE_REG_A) {
				u8 b = (u8[3][8]){
					{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},
					{0x87,0x8F,0x97,0x9F,0xA7,0xAF,0xB7,0xBF},
					{0xC7,0xCF,0xD7,0xDF,0xE7,0xEF,0xF7,0xFF}}[op][bit_id];
				Add_byte_to_output(b);
			}
			else {
				Report_error(Get_token(tok), "Expected 'b', 'c', 'd', 'e', 'h', 'l' or 'a'.");
				return false;
			}
		}
		else {
			u8 b = (u8[3][8]){
				{0x46,0x4E,0x56,0x5E,0x66,0x6E,0x76,0x7E},
				{0x86,0x8E,0x96,0x9E,0xA6,0xAE,0xB6,0xBE},
				{0xC6,0xCE,0xD6,0xDE,0xE6,0xEE,0xF6,0xFE}}[op][bit_id];
			Add_byte_to_output(b);
			Prev_token(tok);
		}
	}
	else {Report_error(Get_token(tok), "Expected number between '0' and '7'."); return false;}
	
	
	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}

static bool
HANDLE_bit(TokenIter *tok) {return HANDLE_bit_res_set(tok, 0);}
static bool
HANDLE_res(TokenIter *tok) {return HANDLE_bit_res_set(tok, 1);}
static bool
HANDLE_set(TokenIter *tok) {return HANDLE_bit_res_set(tok, 2);}





static bool
HANDLE_rlc_rrc_rl_rr_sla_sra_sll_srl(TokenIter *tok, u32 op) {
	Next_token(tok);
	if      (Expect(tok, TOKEN_TYPE_REG_B, NULL)) {Add_word_to_output((u16[]){0x00CB,0x08CB,0x10CB,0x18CB,0x20CB,0x28CB,0x30CB,0x38CB}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_C, NULL)) {Add_word_to_output((u16[]){0x01CB,0x09CB,0x11CB,0x19CB,0x21CB,0x29CB,0x31CB,0x39CB}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_D, NULL)) {Add_word_to_output((u16[]){0x02CB,0x0ACB,0x12CB,0x1ACB,0x22CB,0x2ACB,0x32CB,0x3ACB}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_E, NULL)) {Add_word_to_output((u16[]){0x03CB,0x0BCB,0x13CB,0x1BCB,0x23CB,0x2BCB,0x33CB,0x3BCB}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_H, NULL)) {Add_word_to_output((u16[]){0x04CB,0x0CCB,0x14CB,0x1CCB,0x24CB,0x2CCB,0x34CB,0x3CCB}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_L, NULL)) {Add_word_to_output((u16[]){0x05CB,0x0DCB,0x15CB,0x1DCB,0x25CB,0x2DCB,0x35CB,0x3DCB}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_A, NULL)) {Add_word_to_output((u16[]){0x07CB,0x0FCB,0x17CB,0x1FCB,0x27CB,0x2FCB,0x37CB,0x3FCB}[op]);}
	else if (Expect(tok, TOKEN_TYPE_OPEN_BRACKETS, NULL)) {
		bool is_hl = false;
		Next_token(tok);
		if (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {
			Add_word_to_output((u16[]){0x06CB,0x0ECB,0x16CB,0x1ECB,0x26CB,0x2ECB,0x36CB,0x3ECB}[op]);
			is_hl=true;
		}
		else if (Expect(tok, TOKEN_TYPE_REG_IX, NULL)) {Add_word_to_output(0xCBDD);}
		else if (Expect(tok, TOKEN_TYPE_REG_IY, NULL)) {Add_word_to_output(0xCBFD);}
		else {
			Report_error(Get_token(tok), "Expected '[hl]', '[ix+*]' or '[iy+*]'.");
			return false;
		}
		Next_token(tok);
		if (is_hl) {
			if (!Expect_or_report(tok, TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
			Next_token(tok);
			return Expect_end_of_statement(Get_token(tok));
		}

		Token *curr_tok = Get_token(tok);
		if (curr_tok->type == TOKEN_TYPE_PLUS || curr_tok->type == TOKEN_TYPE_MINUS) {
			u32 token_id = tok->current;
			u32 expr_id  = Parse_expression(tok);
			if (expr_id == 0) {
				Report_error(Get_token(tok), "Cannot parse expression.");
				return false;
			}
			Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_BYTE);
		}
		else {
			Add_byte_to_output(0);
		}

		if (!Expect_or_report(tok, TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}

		Next_token(tok);
		if (Expect(tok, TOKEN_TYPE_COMMA, NULL)) {
			Next_token(tok);
			if      (Expect(tok, TOKEN_TYPE_REG_B, NULL)) {Add_byte_to_output((u8[]){0x00,0x08,0x10,0x18,0x20,0x28,0x30,0x38}[op]);}
			else if (Expect(tok, TOKEN_TYPE_REG_C, NULL)) {Add_byte_to_output((u8[]){0x01,0x09,0x11,0x19,0x21,0x29,0x31,0x39}[op]);}
			else if (Expect(tok, TOKEN_TYPE_REG_D, NULL)) {Add_byte_to_output((u8[]){0x02,0x0A,0x12,0x1A,0x22,0x2A,0x32,0x3A}[op]);}
			else if (Expect(tok, TOKEN_TYPE_REG_E, NULL)) {Add_byte_to_output((u8[]){0x03,0x0B,0x13,0x1B,0x23,0x2B,0x33,0x3B}[op]);}
			else if (Expect(tok, TOKEN_TYPE_REG_H, NULL)) {Add_byte_to_output((u8[]){0x04,0x0C,0x14,0x1C,0x24,0x2C,0x34,0x3C}[op]);}
			else if (Expect(tok, TOKEN_TYPE_REG_L, NULL)) {Add_byte_to_output((u8[]){0x05,0x0D,0x15,0x1D,0x25,0x2D,0x35,0x3D}[op]);}
			else if (Expect(tok, TOKEN_TYPE_REG_A, NULL)) {Add_byte_to_output((u8[]){0x07,0x0F,0x17,0x1F,0x27,0x2F,0x37,0x3F}[op]);}
			else {Report_error(Get_token(tok), "Expected 'b', 'c', 'd', 'e', 'h', 'l' or 'a'."); return false;}
			Next_token(tok);
		}
		else {
			Add_byte_to_output((u8[]){0x06,0x0E,0x16,0x1E,0x26,0x2E,0x36,0x3E}[op]);
		}

		return Expect_end_of_statement(Get_token(tok));
	}
	else {Report_error(Get_token(tok), "Expected 'b', 'c', 'd', 'e', 'h', 'l' or 'a'."); return false;}
	
	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}



static bool
HANDLE_rlc(TokenIter *tok) { return HANDLE_rlc_rrc_rl_rr_sla_sra_sll_srl(tok, 0);}
static bool
HANDLE_rrc(TokenIter *tok) { return HANDLE_rlc_rrc_rl_rr_sla_sra_sll_srl(tok, 1);}
static bool
HANDLE_rl(TokenIter *tok)  { return HANDLE_rlc_rrc_rl_rr_sla_sra_sll_srl(tok, 2);}
static bool
HANDLE_rr(TokenIter *tok)  { return HANDLE_rlc_rrc_rl_rr_sla_sra_sll_srl(tok, 3);}
static bool
HANDLE_sla(TokenIter *tok) { return HANDLE_rlc_rrc_rl_rr_sla_sra_sll_srl(tok, 4);}
static bool
HANDLE_sra(TokenIter *tok) { return HANDLE_rlc_rrc_rl_rr_sla_sra_sll_srl(tok, 5);}
static bool
HANDLE_sll(TokenIter *tok) { return HANDLE_rlc_rrc_rl_rr_sla_sra_sll_srl(tok, 6);}
static bool
HANDLE_srl(TokenIter *tok) { return HANDLE_rlc_rrc_rl_rr_sla_sra_sll_srl(tok, 7);}







static bool
HANDLE_djnz(TokenIter *tok) {
	Add_byte_to_output(0x10);
	Next_token(tok);
	if (Get_token(tok)->type & TOKEN_FLAG_EXPR) {
		u32 token_id = tok->current;
		u32 expr_id = Parse_expression(tok);
		if (expr_id == 0) {
			Report_error(Get_token(tok), "Cannot parse expression.");
			return false;
		}
		Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_REL);
	} else {
		Report_error(Get_token(tok), "Expected expression.");
		return false;
	}

	return Expect_end_of_statement(Get_token(tok));
}





static bool
HANDLE_jr(TokenIter *tok) {
	bool only_addr = false;
	Next_token(tok);
	if      (Expect(tok, TOKEN_TYPE_IDENTIFIER, "nz")) {Add_byte_to_output(0x20);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "nc")) {Add_byte_to_output(0x30);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "z" )) {Add_byte_to_output(0x28);}
	else if (Expect(tok, TOKEN_TYPE_REG_C, NULL))      {Add_byte_to_output(0x38);}
	else if (Get_token(tok)->type & TOKEN_FLAG_EXPR)   {Add_byte_to_output(0x18); only_addr=true;}
	else {
		Report_error(Get_token(tok), "Expected 'nz', 'z', 'nc', 'c' or '**'.");
		return false;
	}
	
	if (!only_addr) {
		Next_token(tok);
		if (!Expect_or_report(tok, TOKEN_TYPE_COMMA, NULL)) {return false;}
		Next_token(tok);
	}
	
	if (Get_token(tok)->type & TOKEN_FLAG_EXPR) {
		u32 token_id = tok->current;
		u32 expr_id  = Parse_expression(tok);
		if (expr_id == 0) {
			Report_error(Get_token(tok), "Cannot parse expression.");
			return false;
		}
		Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_REL);
	}
	else {
		Report_error(Get_token(tok), "Expected placeholder.");
		return false;
	}

	return Expect_end_of_statement(Get_token(tok));
}



static bool
HANDLE_jp(TokenIter *tok) {
	bool only_addr = false;
	Next_token(tok);
	if      (Expect(tok, TOKEN_TYPE_IDENTIFIER, "nz")) {Add_byte_to_output(0xC2);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "z" )) {Add_byte_to_output(0xCA);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "nc")) {Add_byte_to_output(0xD2);}
	else if (Expect(tok, TOKEN_TYPE_REG_C, NULL )) {Add_byte_to_output(0xDA);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "po")) {Add_byte_to_output(0xE2);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "pe")) {Add_byte_to_output(0xEA);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "p" )) {Add_byte_to_output(0xF2);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "m" )) {Add_byte_to_output(0xFA);}
	else if (Get_token(tok)->type & TOKEN_FLAG_EXPR)   {Add_byte_to_output(0xC3); only_addr=true;}
	else if (Expect(tok, TOKEN_TYPE_OPEN_BRACKETS, NULL)) {
		Next_token(tok);
		if      (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {Add_byte_to_output(0xE9);}
		else if (Expect(tok, TOKEN_TYPE_REG_IX, NULL)) {Add_word_to_output(0xE9DD);}
		else if (Expect(tok, TOKEN_TYPE_REG_IY, NULL)) {Add_word_to_output(0xE9FD);}
		else {
			Report_error(Get_token(tok), "Expected '[hl]', '[ix]' or '[iy]'.");
			return false;
		}
		Next_token(tok);
		if (!Expect_or_report(tok, TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
		Next_token(tok);
		return Expect_end_of_statement(Get_token(tok));
	}
	else {
		Report_error(Get_token(tok), "Expected 'nz', 'z', 'nc', 'c', 'po', 'pe', 'p', 'm', [hl], [ix], [iy], or '**'.");
		return false;
	}
	
	if (!only_addr) {
		Next_token(tok);
		if (!Expect_or_report(tok, TOKEN_TYPE_COMMA, NULL)) {return false;}
		Next_token(tok);
	}
	
	if (Get_token(tok)->type & TOKEN_FLAG_EXPR) {
		u32 token_id = tok->current;
		u32 expr_id  = Parse_expression(tok);
		if (expr_id == 0) {
			Report_error(Get_token(tok), "Cannot parse expression.");
			return false;
		}
		Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_WORD);
	}
	else {
		Report_error(Get_token(tok), "Expected placeholder.");
		return false;
	}

	return Expect_end_of_statement(Get_token(tok));
}



static bool
HANDLE_call(TokenIter *tok) {
	bool only_addr = false;
	Next_token(tok);
	if      (Expect(tok, TOKEN_TYPE_IDENTIFIER, "nz")) {Add_byte_to_output(0xC4);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "nc")) {Add_byte_to_output(0xD4);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "po")) {Add_byte_to_output(0xE4);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "p" )) {Add_byte_to_output(0xF4);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "z" )) {Add_byte_to_output(0xCC);}
	else if (Expect(tok, TOKEN_TYPE_REG_C,      NULL)) {Add_byte_to_output(0xDC);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "pe")) {Add_byte_to_output(0xEC);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "m" )) {Add_byte_to_output(0xFC);}
	else if (Get_token(tok)->type & TOKEN_FLAG_EXPR)   {Add_byte_to_output(0xCD); only_addr=true;}
	else {
		Report_error(Get_token(tok), "Expected 'nz', 'nc', 'po', 'p', 'z', 'c', 'pe', 'm' or '**'.");
		return false;
	}
	
	if (!only_addr) {
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA, NULL)) {return false;}
		Next_token(tok);
	}

	if (Get_token(tok)->type & TOKEN_FLAG_EXPR) {
		u32 token_id = tok->current;
		u32 expr_id  = Parse_expression(tok);
		if (expr_id == 0) {
			Report_error(Get_token(tok), "Cannot parse expression.");
			return false;
		}
		Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_WORD);
	}
	else {
		Report_error(Get_token(tok), "Expected placeholder.");
		return false;
	}

	return Expect_end_of_statement(Get_token(tok));
}





static bool
HANDLE_ret(TokenIter *tok) {
	Next_token(tok);
	if      (Expect(tok, TOKEN_TYPE_IDENTIFIER, "nz")) {Add_byte_to_output(0xC0);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "nc")) {Add_byte_to_output(0xD0);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "po")) {Add_byte_to_output(0xE0);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "p" )) {Add_byte_to_output(0xF0);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "z" )) {Add_byte_to_output(0xC8);}
	else if (Expect(tok, TOKEN_TYPE_REG_C,      NULL)) {Add_byte_to_output(0xD8);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "pe")) {Add_byte_to_output(0xE8);}
	else if (Expect(tok, TOKEN_TYPE_IDENTIFIER, "m" )) {Add_byte_to_output(0xF8);}
	else if (Get_token(tok)->type & TOKEN_FLAG_END_OF_STATEMENT)  {Add_byte_to_output(0xC9); return true;}
	else {
		Report_error(Get_token(tok), "Expected 'nz', 'nc', 'po', 'p', 'z', 'c', 'pe', 'm' or 'End of statement'.");
		return false;
	}
	
	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}

static bool
HANDLE_rst(TokenIter *tok) {
	Next_token(tok);
	if      (Expect(tok, TOKEN_TYPE_NUMBER, "00h")) {Add_byte_to_output(0xC7);}
	else if (Expect(tok, TOKEN_TYPE_NUMBER, "10h")) {Add_byte_to_output(0xD7);}
	else if (Expect(tok, TOKEN_TYPE_NUMBER, "20h")) {Add_byte_to_output(0xE7);}
	else if (Expect(tok, TOKEN_TYPE_NUMBER, "30h")) {Add_byte_to_output(0xF7);}
	else if (Expect(tok, TOKEN_TYPE_NUMBER, "08h")) {Add_byte_to_output(0xCF);}
	else if (Expect(tok, TOKEN_TYPE_NUMBER, "18h")) {Add_byte_to_output(0xDF);}
	else if (Expect(tok, TOKEN_TYPE_NUMBER, "28h")) {Add_byte_to_output(0xEF);}
	else if (Expect(tok, TOKEN_TYPE_NUMBER, "38h")) {Add_byte_to_output(0xFF);}
	else {
		Report_error(Get_token(tok), "Expected '00h', '10h', '30h', '08h', '18h', '28h' or '38h'.");
		return false;
	}
	
	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}



static bool
HANDLE_im(TokenIter *tok) {
	if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_NUMBER, NULL)) {return false;}
	s64 val;
	if (!String_to_number(Get_token(tok)->value, Get_token(tok)->value_len, &val)) {
		Report_error(Get_token(tok), "Wrong format number.");
		return false;
	}
	if (val == 0) {
		Next_token(tok);
		if (Expect(tok, TOKEN_TYPE_COMMA, NULL)) {
			if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_NUMBER, NULL)) {return false;}
			s64 val2;
			if (!String_to_number(Get_token(tok)->value, Get_token(tok)->value_len, &val2)) {
				Report_error(Get_token(tok), "Wrong format number.");
				return false;
			}
			if (val2 != 1) {Report_error(Get_token(tok), "Expected a '1'."); return false;}
			Add_word_to_output(0x4EED);
		}
		else {
			if (!(Get_token(tok)->type & TOKEN_FLAG_END_OF_STATEMENT)) {return false;}
			Add_word_to_output(0x46ED);
			return true;
		}
	}
	else if (val == 1) {
		Add_word_to_output(0x56ED);
	}
	else if (val == 2) {
		Add_word_to_output(0x5EED);
	}
	else {Report_error(Get_token(tok), "Expected '0', '1' or '2'."); return false;}

	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}





static bool
HANDLE_ex(TokenIter *tok) {
	Next_token(tok);
	if      (Expect(tok, TOKEN_TYPE_REG_AF, NULL)) {Add_byte_to_output(0x08);}
	else if (Expect(tok, TOKEN_TYPE_REG_DE, NULL)) {
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA,  NULL)) {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_REG_HL, NULL)) {return false;}
		Add_byte_to_output(0xEB);
	}
	else if (Expect(tok, TOKEN_TYPE_OPEN_BRACKETS, NULL)) {
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_REG_SP, NULL)) {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA,          NULL)) {return false;}
		Next_token(tok);
		if      (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {Add_byte_to_output(0xE3);}
		else if (Expect(tok, TOKEN_TYPE_REG_IX, NULL)) {Add_word_to_output(0xE3DD);}
		else if (Expect(tok, TOKEN_TYPE_REG_IY, NULL)) {Add_word_to_output(0xE3FD);}
		else    {Report_error(Get_token(tok), "Expected '[hl]', '[ix]' or '[iy]'."); return false;}
	}
	else {Report_error(Get_token(tok), "Expected 'af', 'de' or '[sp]'."); return false;}
	
	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}









static bool
HANDLE_out(TokenIter *tok) {
	if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_OPEN_BRACKETS, NULL)) {return false;}
	Next_token(tok);
	if (Expect(tok, TOKEN_TYPE_NUMBER, NULL)) {
		u8 b;
		if (!Token_to_byte_or_report(Get_token(tok), &b)) {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA,          NULL)) {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_REG_A,          NULL)) {return false;}
		Add_byte_to_output(0xD3);
		Add_byte_to_output(b);
	}
	else if (Expect(tok, TOKEN_TYPE_REG_C, NULL)) {
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA,          NULL)) {return false;}
		Next_token(tok);
		if      (Expect(tok, TOKEN_TYPE_REG_B, NULL)) {Add_word_to_output(0x41ED);}
		else if (Expect(tok, TOKEN_TYPE_REG_D, NULL)) {Add_word_to_output(0x51ED);}
		else if (Expect(tok, TOKEN_TYPE_REG_H, NULL)) {Add_word_to_output(0x61ED);}
		else if (Expect(tok, TOKEN_TYPE_REG_C, NULL)) {Add_word_to_output(0x49ED);}
		else if (Expect(tok, TOKEN_TYPE_REG_E, NULL)) {Add_word_to_output(0x59ED);}
		else if (Expect(tok, TOKEN_TYPE_REG_L, NULL)) {Add_word_to_output(0x69ED);}
		else if (Expect(tok, TOKEN_TYPE_REG_A, NULL)) {Add_word_to_output(0x79ED);}
		else if (Expect(tok, TOKEN_TYPE_NUMBER, NULL)) {
			u8 b;
			if (!Token_to_byte_or_report(Get_token(tok), &b)) {return false;}
			if (b != 0) {Report_error(Get_token(tok), "Expected '0'."); return false;}
			Add_word_to_output(0x71ED);
		}
		else {
			Report_error(Get_token(tok), "Expected, '0', 'b', 'd', 'h', 'c', 'e', 'l' or 'a'");
			return false;
		}
	}
	else {
		Report_error(Get_token(tok), "Expected '[c]' or '[*]'.");
		return false;
	}
	
	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}






static bool
HANDLE_in(TokenIter *tok) {
	Next_token(tok);
	if (Expect(tok, TOKEN_TYPE_REG_B, NULL)) {
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA, NULL))          {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_OPEN_BRACKETS, NULL))  {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_REG_C, NULL))          {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
		Add_word_to_output(0x40ED);
	}
	else if (Expect(tok, TOKEN_TYPE_REG_D, NULL)) {
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA, NULL))          {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_OPEN_BRACKETS, NULL))  {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_REG_C, NULL))          {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
		Add_word_to_output(0x50ED);
	}
	else if (Expect(tok, TOKEN_TYPE_REG_H, NULL)) {
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA, NULL))          {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_OPEN_BRACKETS, NULL))  {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_REG_C, NULL))          {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
		Add_word_to_output(0x60ED);
	}
	else if (Expect(tok, TOKEN_TYPE_REG_C, NULL)) {
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA, NULL))          {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_OPEN_BRACKETS, NULL))  {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_REG_C, NULL))          {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
		Add_word_to_output(0x48ED);
	}
	else if (Expect(tok, TOKEN_TYPE_REG_E, NULL)) {
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA, NULL))          {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_OPEN_BRACKETS, NULL))  {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_REG_C, NULL))          {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
		Add_word_to_output(0x58ED);
	}
	else if (Expect(tok, TOKEN_TYPE_REG_L, NULL)) {
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA, NULL))          {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_OPEN_BRACKETS, NULL))  {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_REG_C, NULL))          {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
		Add_word_to_output(0x68ED);
	}
	else if (Expect(tok, TOKEN_TYPE_REG_A, NULL)) {
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA, NULL))          {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_OPEN_BRACKETS, NULL))  {return false;}
		Next_token(tok);
		if (Expect(tok, TOKEN_TYPE_REG_C, NULL)) {Add_word_to_output(0x78ED);}
		else if (Expect(tok, TOKEN_TYPE_NUMBER, NULL)) {
			u8 b;
			if (!Token_to_byte_or_report(Get_token(tok), &b)) {return false;}
			Add_byte_to_output(0xDB);
			Add_byte_to_output(b);
		}
		else {Report_error(Get_token(tok), "Expected '[c]' or '[*]'."); return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
	}
	else if (Expect(tok, TOKEN_TYPE_OPEN_BRACKETS, NULL)) {
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_REG_C, NULL))      {return false;}
		if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
		Add_word_to_output(0x70ED);
	}
	else {
		Report_error(Get_token(tok), "Expected 'b', 'd', 'h', 'c', 'e', 'l', 'a' or '[c].'");
		return false;
	}
	
	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}






enum {
	AOP_SUB = 0,
	AOP_OR  = 1,
	AOP_AND = 2,
	AOP_XOR = 3,
	AOP_CP  = 4,
	AOP_ADD = 5,
	AOP_ADC = 6,
	AOP_SBC = 7,
};


static bool
Parse_acum_operation(TokenIter *tok, int op) {
	Next_token(tok);

	//                                                                         sub  or   and   xor cp   add  adc   sbc
	if      (Expect(tok, TOKEN_TYPE_REG_B,   NULL)) {Add_byte_to_output((u8[]){0x90,0xB0,0xA0,0xA8,0xB8,0x80,0x88,0x98}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_C,   NULL)) {Add_byte_to_output((u8[]){0x91,0xB1,0xA1,0xA9,0xB9,0x81,0x89,0x99}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_D,   NULL)) {Add_byte_to_output((u8[]){0x92,0xB2,0xA2,0xAA,0xBA,0x82,0x8A,0x9A}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_E,   NULL)) {Add_byte_to_output((u8[]){0x93,0xB3,0xA3,0xAB,0xBB,0x83,0x8B,0x9B}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_H,   NULL)) {Add_byte_to_output((u8[]){0x94,0xB4,0xA4,0xAC,0xBC,0x84,0x8C,0x9C}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_L,   NULL)) {Add_byte_to_output((u8[]){0x95,0xB5,0xA5,0xAD,0xBD,0x85,0x8D,0x9D}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_A,   NULL)) {Add_byte_to_output((u8[]){0x97,0xB7,0xA7,0xAF,0xBF,0x87,0x8F,0x9F}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_IXH, NULL)) {Add_word_to_output((u16[]){0x94DD,0xB4DD,0xA4DD,0xACDD,0xBCDD,0x84DD,0x8CDD,0x9CDD}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_IXL, NULL)) {Add_word_to_output((u16[]){0x95DD,0xB5DD,0xA5DD,0xADDD,0xBDDD,0x85DD,0x8DDD,0x9DDD}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_IYH, NULL)) {Add_word_to_output((u16[]){0x94FD,0xB4FD,0xA4FD,0xACFD,0xBCFD,0x84FD,0x8CFD,0x9CFD}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_IYL, NULL)) {Add_word_to_output((u16[]){0x95FD,0xB5FD,0xA5FD,0xADFD,0xBDFD,0x85FD,0x8DFD,0x9DFD}[op]);}
	else if (Get_token(tok)->type & TOKEN_FLAG_EXPR) {
		Add_byte_to_output((u8[]){0xD6,0xF6,0xE6,0xEE,0xFE,0xC6,0xCE,0xDE}[op]);
		u32 token_id = tok->current;
		u32 expr_id = Parse_expression(tok);
		if (expr_id == 0) {
			Report_error(Get_token(tok), "Cannot parse expression.");
			return false;
		}
		Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_BYTE);
		Prev_token(tok);
	}
	else if (Expect(tok, TOKEN_TYPE_OPEN_BRACKETS, NULL)) {
		Next_token(tok);
		if (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {
			Add_byte_to_output((u8[]){0x96,0xB6,0xA6,0xAE,0xBE,0x86,0x8E,0x9E}[op]);
			Next_token(tok);
		}
		else {
			if (Expect(tok, TOKEN_TYPE_REG_IX, NULL)) {
				Add_word_to_output((u16[]){0x96DD,0xB6DD,0xA6DD,0xAEDD,0xBEDD,0x86DD,0x8EDD,0x9EDD}[op]);
			}
			else if (Expect(tok, TOKEN_TYPE_REG_IY, NULL)) {
				Add_word_to_output((u16[]){0x96FD,0xB6FD,0xA6FD,0xAEFD,0xBEFD,0x86FD,0x8EFD,0x9EFD}[op]);
			}
			else {
				Report_error(Get_token(tok), "Expected '[hl]', '[ix+*]' or '[iy+*]'.");
				return false;
			}
			Next_token(tok);
			Token *curr_tok = Get_token(tok);
			if (curr_tok->type == TOKEN_TYPE_PLUS || curr_tok->type == TOKEN_TYPE_MINUS) {
				u32 token_id = tok->current;
				u32 expr_id  = Parse_expression(tok);
				if (expr_id == 0) {
					Report_error(Get_token(tok), "Cannot parse expression.");
					return false;
				}
				Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_BYTE);
			}
			else {
				Add_byte_to_output(0);
			}
		}

		if (!Expect_or_report(tok, TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
	}
	else {
		Report_error(Get_token(tok), "Expected 'b', 'c', 'd', 'e', 'h', 'l', '[hl]', 'a', '*', 'ixh', 'ixl', '[ix+*]', 'iyh', 'iyl' or [iy+*].");
		return false;
	}
	
	
	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));

}



static bool
HANDLE_add(TokenIter *tok) {
	Next_token(tok);

	if (Expect(tok, TOKEN_TYPE_REG_A, NULL)) {
		Next_token(tok);
		if (!Expect_or_report(tok, TOKEN_TYPE_COMMA, NULL)) {return false;}
		return Parse_acum_operation(tok, AOP_ADD);
	}
	else if (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {
		Next_token(tok);
		if (!Expect_or_report(tok, TOKEN_TYPE_COMMA, NULL)) {return false;}
		Next_token(tok);
		if      (Expect(tok, TOKEN_TYPE_REG_BC, NULL)) {Add_byte_to_output(0x09);}
		else if (Expect(tok, TOKEN_TYPE_REG_DE, NULL)) {Add_byte_to_output(0x19);}
		else if (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {Add_byte_to_output(0x29);}
		else if (Expect(tok, TOKEN_TYPE_REG_SP, NULL)) {Add_byte_to_output(0x39);}
		else {
			Report_error(Get_token(tok), "Expected 'bc', 'de', 'hl' or 'sp'.");
			return false;
		}

	} else if (Expect(tok, TOKEN_TYPE_REG_IX, NULL)) {
		Next_token(tok);
		if (!Expect_or_report(tok, TOKEN_TYPE_COMMA, NULL)) {return false;}
		Next_token(tok);
		if      (Expect(tok, TOKEN_TYPE_REG_BC, NULL)) {Add_word_to_output(0x09DD);}
		else if (Expect(tok, TOKEN_TYPE_REG_DE, NULL)) {Add_word_to_output(0x19DD);}
		else if (Expect(tok, TOKEN_TYPE_REG_IX, NULL)) {Add_word_to_output(0x29DD);}
		else if (Expect(tok, TOKEN_TYPE_REG_SP, NULL)) {Add_word_to_output(0x39DD);}
		else {
			Report_error(Get_token(tok), "Expected 'bc', 'de', 'ix' or 'sp'.");
			return false;
		}

	} else if (Expect(tok, TOKEN_TYPE_REG_IY, NULL)) {
		Next_token(tok);
		if (!Expect_or_report(tok, TOKEN_TYPE_COMMA, NULL)) {return false;}
		Next_token(tok);
		if      (Expect(tok, TOKEN_TYPE_REG_BC, NULL)) {Add_word_to_output(0x09FD);}
		else if (Expect(tok, TOKEN_TYPE_REG_DE, NULL)) {Add_word_to_output(0x19FD);}
		else if (Expect(tok, TOKEN_TYPE_REG_IY, NULL)) {Add_word_to_output(0x29FD);}
		else if (Expect(tok, TOKEN_TYPE_REG_SP, NULL)) {Add_word_to_output(0x39FD);}
		else {
			Report_error(Get_token(tok), "Expected 'bc', 'de', 'iy' or 'sp'.");
			return false;
		}

	} else {
		Report_error(Get_token(tok), "Expected a register 'a', 'hl', 'ix' or 'iy'.");
		return false;
	}
	
	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}


static bool
HANDLE_adc(TokenIter *tok) {
	Next_token(tok);

	if (Expect(tok, TOKEN_TYPE_REG_A, NULL)) {
		Next_token(tok);
		if (!Expect_or_report(tok, TOKEN_TYPE_COMMA, NULL)) {return false;}
		return Parse_acum_operation(tok, AOP_ADC);
	}
	else if (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {
		Next_token(tok);
		if (!Expect_or_report(tok, TOKEN_TYPE_COMMA, NULL)) {return false;}
		Next_token(tok);
		if      (Expect(tok, TOKEN_TYPE_REG_BC, NULL)) {Add_word_to_output(0x4AED);}
		else if (Expect(tok, TOKEN_TYPE_REG_DE, NULL)) {Add_word_to_output(0x5AED);}
		else if (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {Add_word_to_output(0x6AED);}
		else if (Expect(tok, TOKEN_TYPE_REG_SP, NULL)) {Add_word_to_output(0x7AED);}
		else {
			Report_error(Get_token(tok), "Expected 'bc', 'de', 'hl' or 'sp'.");
			return false;
		}

	}
	else {
		Report_error(Get_token(tok), "Expected a register 'a' or 'hl'.");
		return false;
	}
	
	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}



// FIXME copypaste from ADC (Well, but both functions are pretty small)
static bool
HANDLE_sbc(TokenIter *tok) {
	Next_token(tok);

	if (Expect(tok, TOKEN_TYPE_REG_A, NULL)) {
		Next_token(tok);
		if (!Expect_or_report(tok, TOKEN_TYPE_COMMA, NULL)) {return false;}
		return Parse_acum_operation(tok, AOP_SBC);
	}
	else if (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {
		Next_token(tok);
		if (!Expect_or_report(tok, TOKEN_TYPE_COMMA, NULL)) {return false;}
		Next_token(tok);
		if      (Expect(tok, TOKEN_TYPE_REG_BC, NULL)) {Add_word_to_output(0x42ED);}
		else if (Expect(tok, TOKEN_TYPE_REG_DE, NULL)) {Add_word_to_output(0x52ED);}
		else if (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {Add_word_to_output(0x62ED);}
		else if (Expect(tok, TOKEN_TYPE_REG_SP, NULL)) {Add_word_to_output(0x72ED);}
		else {
			Report_error(Get_token(tok), "Expected 'bc', 'de', 'hl' or 'sp'.");
			return false;
		}

	}
	else {
		Report_error(Get_token(tok), "Expected a register 'a' or 'hl'.");
		return false;
	}
	
	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}


static bool
HANDLE_ld__a__X(TokenIter *tok) {
	Next_token(tok);
	if (!Expect_or_report(tok, TOKEN_TYPE_COMMA, NULL)) {return false;}
	Next_token(tok);
	if      (Expect(tok, TOKEN_TYPE_REG_B,   NULL)) {Add_byte_to_output(0x78);}
	else if (Expect(tok, TOKEN_TYPE_REG_C,   NULL)) {Add_byte_to_output(0x79);}
	else if (Expect(tok, TOKEN_TYPE_REG_D,   NULL)) {Add_byte_to_output(0x7A);}
	else if (Expect(tok, TOKEN_TYPE_REG_E,   NULL)) {Add_byte_to_output(0x7B);}
	else if (Expect(tok, TOKEN_TYPE_REG_H,   NULL)) {Add_byte_to_output(0x7C);}
	else if (Expect(tok, TOKEN_TYPE_REG_L,   NULL)) {Add_byte_to_output(0x7D);}
	else if (Expect(tok, TOKEN_TYPE_REG_A,   NULL)) {Add_byte_to_output(0x7F);}
	else if (Expect(tok, TOKEN_TYPE_REG_I,   NULL)) {Add_word_to_output(0x57ED);}
	else if (Expect(tok, TOKEN_TYPE_REG_R,   NULL)) {Add_word_to_output(0x5FED);}
	else if (Expect(tok, TOKEN_TYPE_REG_IXH, NULL)) {Add_word_to_output(0x7CDD);}
	else if (Expect(tok, TOKEN_TYPE_REG_IXL, NULL)) {Add_word_to_output(0x7DDD);}
	else if (Expect(tok, TOKEN_TYPE_REG_IYH, NULL)) {Add_word_to_output(0x7CFD);}
	else if (Expect(tok, TOKEN_TYPE_REG_IYL, NULL)) {Add_word_to_output(0x7DFD);}
	else if (Get_token(tok)->type & TOKEN_FLAG_EXPR) {
		Add_byte_to_output(0x3E);
		u32 token_id = tok->current;
		u32 expr_id = Parse_expression(tok);
		if (expr_id == 0) {
			Report_error(Get_token(tok), "Cannot parse expression.");
			return false;
		}
		Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_BYTE);
		Prev_token(tok);
	}
	else if (Expect(tok, TOKEN_TYPE_OPEN_BRACKETS, NULL)) {
		Next_token(tok);
		if      (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {Add_byte_to_output(0x7E);}
		else if (Expect(tok, TOKEN_TYPE_REG_BC, NULL)) {Add_byte_to_output(0x0A);}
		else if (Expect(tok, TOKEN_TYPE_REG_DE, NULL)) {Add_byte_to_output(0x1A);}
		else if (Get_token(tok)->type & TOKEN_FLAG_EXPR) {
			Add_byte_to_output(0x3A);
			u32 token_id = tok->current;
			u32 expr_id = Parse_expression(tok);
			if (expr_id == 0) {
				Report_error(Get_token(tok), "Cannot parse expression.");
				return false;
			}
			Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_WORD);
			Prev_token(tok);
		}
		else {
			if      (Expect(tok, TOKEN_TYPE_REG_IX, NULL)) {Add_word_to_output(0x7EDD);}
			else if (Expect(tok, TOKEN_TYPE_REG_IY, NULL)) {Add_word_to_output(0x7EFD);}
			else {
				Report_error(Get_token(tok), "Expected '[**]', '[hl]', '[bc]', '[de]', '[ix+*]' or '[iy+*]'.");
				return false;
			}
			Next_token(tok);
			Token *curr_tok = Get_token(tok);
			if (curr_tok->type == TOKEN_TYPE_PLUS || curr_tok->type == TOKEN_TYPE_MINUS) {
				u32 token_id = tok->current;
				u32 expr_id  = Parse_expression(tok);
				if (expr_id == 0) {
					Report_error(Get_token(tok), "Cannot parse expression.");
					return false;
				}
				Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_BYTE);
			}
			else {
				Add_byte_to_output(0);
			}
			Prev_token(tok);
		}
		Next_token(tok);
		if (!Expect_or_report(tok, TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
	}
	else {
		Report_error(Get_token(tok), "Expected 'b', 'c', 'd', 'e', 'h', 'l', 'a', 'i', 'r', 'ixh', ixl', 'iyh', 'iyl', '*', '[**]', '[hl]', '[bc]', '[de]', '[ix+*]' or '[iy+*]'.");
		return false;

	}

	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}


static bool
HANDLE_ld__bc_de_hl_ix_iy__XX(TokenIter *tok, u32 op) {
	if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA, NULL)) {return false;};
	Next_token(tok);
	if (Get_token(tok)->type & TOKEN_FLAG_EXPR) {
		Add_byte_or_word_to_output((u16[]){0x01,0x11,0x21,0x21DD,0x21FD}[op]);
		u32 token_id = tok->current;
		u32 expr_id = Parse_expression(tok);
		if (expr_id == 0) {
			Report_error(Get_token(tok), "Cannot parse expression.");
			return false;
		}
		Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_WORD);
		Prev_token(tok);
	}
	else if (Expect(tok, TOKEN_TYPE_OPEN_BRACKETS, NULL)) {
		Next_token(tok);
		if (!(Get_token(tok)->type & TOKEN_FLAG_EXPR)) {
			Report_error(Get_token(tok), "Expected, [**].");
			return false;
		}
		Add_byte_or_word_to_output((u16[]){0x4BED,0x5BED,0x2A,0x2ADD,0x2AFD}[op]);
		u32 token_id = tok->current;
		u32 expr_id = Parse_expression(tok);
		if (expr_id == 0) {
			Report_error(Get_token(tok), "Cannot parse expression.");
			return false;
		}
		Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_WORD);
		if (!Expect_or_report(tok, TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
	}
	else {
		Report_error(Get_token(tok), "Expected '**' or '[**]'.");
	}

	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}



static bool
HANDLE_ld__iXX__X(TokenIter *tok, u32 op) {
	Next_token(tok);
	if (!Expect_or_report(tok, TOKEN_TYPE_COMMA, NULL)) {return false;}
	Next_token(tok);
	if      (Expect(tok, TOKEN_TYPE_REG_B, NULL)) {Add_word_to_output((u16[]){0x60DD,0x68DD,0x60FD,0x68FD}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_C, NULL)) {Add_word_to_output((u16[]){0x61DD,0x69DD,0x61FD,0x69FD}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_D, NULL)) {Add_word_to_output((u16[]){0x62DD,0x6ADD,0x62FD,0x6AFD}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_E, NULL)) {Add_word_to_output((u16[]){0x63DD,0x6BDD,0x63FD,0x6BFD}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_A, NULL)) {Add_word_to_output((u16[]){0x67DD,0x6FDD,0x67FD,0x6FFD}[op]);}
	else if (op<2 && Expect(tok, TOKEN_TYPE_REG_IXH, NULL)) {Add_word_to_output((u16[]){0x64DD,0x6CDD,0x0000,0x0000}[op]);}
	else if (op<2 && Expect(tok, TOKEN_TYPE_REG_IXL, NULL)) {Add_word_to_output((u16[]){0x65DD,0x6DDD,0x0000,0x0000}[op]);}
	else if (op>1 && Expect(tok, TOKEN_TYPE_REG_IYH, NULL)) {Add_word_to_output((u16[]){0x0000,0x0000,0x64FD,0x6CFD}[op]);}
	else if (op>1 && Expect(tok, TOKEN_TYPE_REG_IYL, NULL)) {Add_word_to_output((u16[]){0x0000,0x0000,0x65FD,0x6DFD}[op]);}
	else if (Get_token(tok)->type & TOKEN_FLAG_EXPR) {
		Add_word_to_output((u16[]){0x26DD,0x2EDD,0x26FD,0x2EFD}[op]);
		u32 token_id = tok->current;
		u32 expr_id = Parse_expression(tok);
		if (expr_id == 0) {
			Report_error(Get_token(tok), "Cannot parse expression.");
			return false;
		}
		Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_BYTE);
		Prev_token(tok);
	}
	else if (op < 2) {Report_error(Get_token(tok), "Expected 'b', 'c', 'd', 'e', 'a', 'ixh', 'ixl' or '*'.");}
	else             {Report_error(Get_token(tok), "Expected 'b', 'c', 'd', 'e', 'a', 'iyh', 'iyl' or '*'.");}

	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}

static bool
HANDLE_ld__sp__XX(TokenIter *tok) {
	Next_token(tok);
	if (!Expect_or_report(tok, TOKEN_TYPE_COMMA, NULL)) {return false;}
	Next_token(tok);
	if      (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {Add_byte_to_output(0xF9);}
	else if (Expect(tok, TOKEN_TYPE_REG_IX, NULL)) {Add_word_to_output(0xF9DD);}
	else if (Expect(tok, TOKEN_TYPE_REG_IY, NULL)) {Add_word_to_output(0xF9FD);}
	else if (Get_token(tok)->type & TOKEN_FLAG_EXPR) {
		Add_byte_to_output(0x31);
		u32 token_id = tok->current;
		u32 expr_id = Parse_expression(tok);
		if (expr_id == 0) {
			Report_error(Get_token(tok), "Cannot parse expression.");
			return false;
		}
		Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_WORD);
		Prev_token(tok);
	}
	else if (Expect(tok, TOKEN_TYPE_OPEN_BRACKETS, NULL)) {
		Next_token(tok);
		if (!(Get_token(tok)->type & TOKEN_FLAG_EXPR)) {
			Report_error(Get_token(tok), "Expected [**].");
		}
		Add_word_to_output(0x7BED);
		u32 token_id = tok->current;
		u32 expr_id = Parse_expression(tok);
		if (expr_id == 0) {
			Report_error(Get_token(tok), "Cannot parse expression.");
			return false;
		}
		Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_WORD);
		if (!Expect_or_report(tok, TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
	}
	else {
		Report_error(Get_token(tok), "Expected 'hl', 'ix', 'iy', '**' or '[**].");
		return false;
	}

	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}



static bool
HANDLE_ld__b_c_d_e__X(TokenIter *tok, u32 op) {
	Next_token(tok);
	if (!Expect_or_report(tok, TOKEN_TYPE_COMMA, NULL)) {return false;}
	Next_token(tok);
	if      (Expect(tok, TOKEN_TYPE_REG_B, NULL))   {Add_byte_to_output((u8[]){0x40,0x48,0x50,0x58}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_C, NULL))   {Add_byte_to_output((u8[]){0x41,0x49,0x51,0x59}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_D, NULL))   {Add_byte_to_output((u8[]){0x42,0x4A,0x52,0x5A}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_E, NULL))   {Add_byte_to_output((u8[]){0x43,0x4B,0x53,0x5B}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_H, NULL))   {Add_byte_to_output((u8[]){0x44,0x4C,0x54,0x5C}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_L, NULL))   {Add_byte_to_output((u8[]){0x45,0x4D,0x55,0x5D}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_A, NULL))   {Add_byte_to_output((u8[]){0x47,0x4F,0x57,0x5F}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_IXH, NULL)) {Add_word_to_output((u16[]){0x44DD,0x4CDD,0x54DD,0x5CDD}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_IXL, NULL)) {Add_word_to_output((u16[]){0x45DD,0x4DDD,0x55DD,0x5DDD}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_IYH, NULL)) {Add_word_to_output((u16[]){0x44FD,0x4CFD,0x54FD,0x5CFD}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_IYL, NULL)) {Add_word_to_output((u16[]){0x45FD,0x4DFD,0x55FD,0x5DFD}[op]);}
	else if (Get_token(tok)->type & TOKEN_FLAG_EXPR) {
		Add_byte_to_output((u8[]){0x06,0x0E,0x16,0x1E}[op]);
		u32 token_id = tok->current;
		u32 expr_id = Parse_expression(tok);
		if (expr_id == 0) {
			Report_error(Get_token(tok), "Cannot parse expression.");
			return false;
		}
		Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_BYTE);
		Prev_token(tok);
	}
	else if (Expect(tok, TOKEN_TYPE_OPEN_BRACKETS, NULL)) {

		Next_token(tok);
		if      (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {Add_byte_to_output((u8[]){0x46,0x4E,0x56,0x5E}[op]); Next_token(tok);}
		else {
			if      (Expect(tok, TOKEN_TYPE_REG_IX, NULL)) {Add_word_to_output((u16[]){0x46DD,0x4EDD,0x56DD,0x5EDD}[op]);}
			else if (Expect(tok, TOKEN_TYPE_REG_IY, NULL)) {Add_word_to_output((u16[]){0x46FD,0x4EFD,0x56FD,0x5EFD}[op]);}
			else {
				Report_error(Get_token(tok), "Expected '[hl]', '[ix+*]' or '[iy+*]'.");
				return false;
			}
			Next_token(tok);
			Token *curr_tok = Get_token(tok);
			if (curr_tok->type == TOKEN_TYPE_PLUS || curr_tok->type == TOKEN_TYPE_MINUS) {
				u32 token_id = tok->current;
				u32 expr_id  = Parse_expression(tok);
				if (expr_id == 0) {
					Report_error(Get_token(tok), "Cannot parse expression.");
					return false;
				}
				Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_BYTE);
			}
			else {
				Add_byte_to_output(0);
			}
		}
		if (!Expect_or_report(tok, TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
	}
	else {
		Report_error(Get_token(tok), "Expected 'b', 'c', 'd', 'e', 'h', 'l', 'a', 'ixh', ixl', 'iyh', 'iyl', '*', '[hl]', '[ix+*]' or '[iy+*]'.");
		return false;
	}

	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}



static bool
HANDLE_ld__h_l__X(TokenIter *tok, u32 op) {
	Next_token(tok);
	if (!Expect_or_report(tok, TOKEN_TYPE_COMMA, NULL)) {return false;}
	Next_token(tok);
	if      (Expect(tok, TOKEN_TYPE_REG_B, NULL))   {Add_byte_to_output((u8[]){0x60,0x68}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_C, NULL))   {Add_byte_to_output((u8[]){0x61,0x69}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_D, NULL))   {Add_byte_to_output((u8[]){0x62,0x6A}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_E, NULL))   {Add_byte_to_output((u8[]){0x63,0x6B}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_H, NULL))   {Add_byte_to_output((u8[]){0x64,0x6C}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_L, NULL))   {Add_byte_to_output((u8[]){0x65,0x6D}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_A, NULL))   {Add_byte_to_output((u8[]){0x67,0x6F}[op]);}
	else if (Get_token(tok)->type & TOKEN_FLAG_EXPR) {
		Add_byte_to_output((u8[]){0x26,0x2E}[op]);
		u32 token_id = tok->current;
		u32 expr_id = Parse_expression(tok);
		if (expr_id == 0) {
			Report_error(Get_token(tok), "Cannot parse expression.");
			return false;
		}
		Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_BYTE);
		Prev_token(tok);
	}
	else if (Expect(tok, TOKEN_TYPE_OPEN_BRACKETS, NULL)) {

		Next_token(tok);
		if      (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {Add_byte_to_output((u8[]){0x66,0x6E}[op]);}
		else {
			if      (Expect(tok, TOKEN_TYPE_REG_IX, NULL)) {Add_word_to_output((u16[]){0x66DD,0x6EDD}[op]);}
			else if (Expect(tok, TOKEN_TYPE_REG_IY, NULL)) {Add_word_to_output((u16[]){0x66FD,0x6EFD}[op]);}
			else {
				Report_error(Get_token(tok), "Expected '[hl]', '[ix+*]' or '[iy+*]'.");
				return false;
			}
			Next_token(tok);
			Token *curr_tok = Get_token(tok);
			if (curr_tok->type == TOKEN_TYPE_PLUS || curr_tok->type == TOKEN_TYPE_MINUS) {
				u32 token_id = tok->current;
				u32 expr_id  = Parse_expression(tok);
				if (expr_id == 0) {
					Report_error(Get_token(tok), "Cannot parse expression.");
					return false;
				}
				Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_BYTE);
			}
			else {
				Add_byte_to_output(0);
			}
			Prev_token(tok);
		}

		Next_token(tok);
		if (!Expect_or_report(tok, TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
	}
	else {
		Report_error(Get_token(tok), "Expected 'b', 'c', 'd', 'e', 'h', 'l', 'a', 'ixh', ixl', 'iyh', 'iyl', '*', '[hl]', '[ix+*]' or '[iy+*]'.");
		return false;
	}

	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}


static bool
HANDLE_ld__bracket_ix_or_iy(TokenIter *tok, u32 op) {
	Next_token(tok);
	u32 offset_expr_id  = 0;
	u32 offset_token_id = 0;
	u32 inmediate_expr_id  = 0;
	u32 inmediate_token_id = 0;
	Token *curr_tok = Get_token(tok);
	if (curr_tok->type == TOKEN_TYPE_PLUS || curr_tok->type == TOKEN_TYPE_MINUS) {
		offset_token_id = tok->current;
		offset_expr_id  = Parse_expression(tok);
		if (offset_expr_id == 0) {
			Report_error(Get_token(tok), "Cannot parse expression.");
			return false;
		}
	}
	if (!Expect_or_report(tok, TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
	if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA, NULL)) {return false;}
	Next_token(tok);
	if      (Expect(tok, TOKEN_TYPE_REG_B, NULL)) {Add_word_to_output((u16[]){0x70DD, 0x70FD}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_C, NULL)) {Add_word_to_output((u16[]){0x71DD, 0x71FD}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_D, NULL)) {Add_word_to_output((u16[]){0x72DD, 0x72FD}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_E, NULL)) {Add_word_to_output((u16[]){0x73DD, 0x73FD}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_H, NULL)) {Add_word_to_output((u16[]){0x74DD, 0x74FD}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_L, NULL)) {Add_word_to_output((u16[]){0x75DD, 0x75FD}[op]);}
	else if (Expect(tok, TOKEN_TYPE_REG_A, NULL)) {Add_word_to_output((u16[]){0x77DD, 0x77FD}[op]);}
	else if (Get_token(tok)->type & TOKEN_FLAG_EXPR) {
		Add_word_to_output((u16[]){0x36DD, 0x36FD}[op]);
		inmediate_token_id = tok->current;
		inmediate_expr_id = Parse_expression(tok);
		if (inmediate_expr_id == 0) {
			Report_error(Get_token(tok), "Cannot parse expression.");
			return false;
		}
		Prev_token(tok);
	}
	else {Report_error(Get_token(tok), "Expected 'b', 'c', 'd', 'e', 'h', 'l', 'a' or '*'"); return false;}


	if (offset_expr_id != 0) {
		Add_placeholder(offset_token_id, offset_expr_id, PLACEHOLDER_TYPE_BYTE);
	}
	else {
		Add_byte_to_output(0);
	}
	if (inmediate_expr_id != 0) {
		Add_placeholder(inmediate_token_id, inmediate_expr_id, PLACEHOLDER_TYPE_BYTE);
	}
	return true;
}


static bool
HANDLE_ld(TokenIter *tok) {
	Next_token(tok);
	if      (Expect(tok, TOKEN_TYPE_REG_A,   NULL)) {return HANDLE_ld__a__X(tok);}
	else if (Expect(tok, TOKEN_TYPE_REG_B,   NULL)) {return HANDLE_ld__b_c_d_e__X(tok, 0);}
	else if (Expect(tok, TOKEN_TYPE_REG_C,   NULL)) {return HANDLE_ld__b_c_d_e__X(tok, 1);}
	else if (Expect(tok, TOKEN_TYPE_REG_D,   NULL)) {return HANDLE_ld__b_c_d_e__X(tok, 2);}
	else if (Expect(tok, TOKEN_TYPE_REG_E,   NULL)) {return HANDLE_ld__b_c_d_e__X(tok, 3);}
	else if (Expect(tok, TOKEN_TYPE_REG_H,   NULL)) {return HANDLE_ld__h_l__X(tok, 0);}
	else if (Expect(tok, TOKEN_TYPE_REG_L,   NULL)) {return HANDLE_ld__h_l__X(tok, 1);}
	else if (Expect(tok, TOKEN_TYPE_REG_IXH, NULL)) {return HANDLE_ld__iXX__X(tok, 0);}
	else if (Expect(tok, TOKEN_TYPE_REG_IXL, NULL)) {return HANDLE_ld__iXX__X(tok, 1);}
	else if (Expect(tok, TOKEN_TYPE_REG_IYH, NULL)) {return HANDLE_ld__iXX__X(tok, 2);}
	else if (Expect(tok, TOKEN_TYPE_REG_IYL, NULL)) {return HANDLE_ld__iXX__X(tok, 3);}
	else if (Expect(tok, TOKEN_TYPE_REG_SP,  NULL)) {return HANDLE_ld__sp__XX(tok);}
	else if (Expect(tok, TOKEN_TYPE_REG_BC,  NULL)) {return HANDLE_ld__bc_de_hl_ix_iy__XX(tok, 0);}
	else if (Expect(tok, TOKEN_TYPE_REG_DE,  NULL)) {return HANDLE_ld__bc_de_hl_ix_iy__XX(tok, 1);}
	else if (Expect(tok, TOKEN_TYPE_REG_HL,  NULL)) {return HANDLE_ld__bc_de_hl_ix_iy__XX(tok, 2);}
	else if (Expect(tok, TOKEN_TYPE_REG_IX,  NULL)) {return HANDLE_ld__bc_de_hl_ix_iy__XX(tok, 3);}
	else if (Expect(tok, TOKEN_TYPE_REG_IY,  NULL)) {return HANDLE_ld__bc_de_hl_ix_iy__XX(tok, 4);}
	else if (Expect(tok, TOKEN_TYPE_OPEN_BRACKETS, NULL)) {
		Next_token(tok);
		if (Expect(tok, TOKEN_TYPE_REG_BC, NULL)) {
			if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
			if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA, NULL)) {return false;}
			if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_REG_A, NULL)) {return false;}
			Add_byte_to_output(0x02);
		}
		else if (Expect(tok, TOKEN_TYPE_REG_DE, NULL)) {
			if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
			if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA, NULL)) {return false;}
			if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_REG_A, NULL)) {return false;}
			Add_byte_to_output(0x12);
		}
		else if (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {
			if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
			if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA, NULL)) {return false;}
			Next_token(tok);
			if      (Expect(tok, TOKEN_TYPE_REG_B, NULL)) {Add_byte_to_output(0x70);}
			else if (Expect(tok, TOKEN_TYPE_REG_C, NULL)) {Add_byte_to_output(0x71);}
			else if (Expect(tok, TOKEN_TYPE_REG_D, NULL)) {Add_byte_to_output(0x72);}
			else if (Expect(tok, TOKEN_TYPE_REG_E, NULL)) {Add_byte_to_output(0x73);}
			else if (Expect(tok, TOKEN_TYPE_REG_H, NULL)) {Add_byte_to_output(0x74);}
			else if (Expect(tok, TOKEN_TYPE_REG_L, NULL)) {Add_byte_to_output(0x75);}
			else if (Expect(tok, TOKEN_TYPE_REG_A, NULL)) {Add_byte_to_output(0x77);}
			else if (Get_token(tok)->type & TOKEN_FLAG_EXPR) {
				Add_byte_to_output(0x36);
				u32 token_id = tok->current;
				u32 expr_id = Parse_expression(tok);
				if (expr_id == 0) {
					Report_error(Get_token(tok), "Cannot parse expression.");
					return false;
				}
				Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_BYTE);
				Prev_token(tok);
			}
			else {Report_error(Get_token(tok), "Expected 'b', 'c', 'd', 'e', 'h', 'l', 'a' or '*'"); return false;}
		}
		else if (Expect(tok, TOKEN_TYPE_REG_IX, NULL)) {
			if(!HANDLE_ld__bracket_ix_or_iy(tok, 0)) {return false;}
		}
		else if (Expect(tok, TOKEN_TYPE_REG_IY, NULL)) {
			if(!HANDLE_ld__bracket_ix_or_iy(tok, 1)) {return false;}
		}
		else if (Expect(tok, TOKEN_TYPE_NUMBER, NULL) || Expect(tok, TOKEN_TYPE_IDENTIFIER, NULL)) {
			u32 token_id = tok->current;
			u32 expr_id = Parse_expression(tok);
			if (expr_id == 0) {
				Report_error(Get_token(tok), "Cannot parse expression.");
				return false;
			}
			if (!Expect_or_report(tok, TOKEN_TYPE_CLOSE_BRACKETS, NULL)) {return false;}
			if (!Expect_or_report(Next_token(tok), TOKEN_TYPE_COMMA, NULL)) {return false;}
			Next_token(tok);
			if      (Expect(tok, TOKEN_TYPE_REG_A,  NULL)) {Add_byte_to_output(0x32);}
			else if (Expect(tok, TOKEN_TYPE_REG_HL, NULL)) {Add_byte_to_output(0x22);}
			else if (Expect(tok, TOKEN_TYPE_REG_BC, NULL)) {Add_word_to_output(0x43ED);}
			else if (Expect(tok, TOKEN_TYPE_REG_DE, NULL)) {Add_word_to_output(0x53ED);}
			else if (Expect(tok, TOKEN_TYPE_REG_SP, NULL)) {Add_word_to_output(0x73ED);}
			else if (Expect(tok, TOKEN_TYPE_REG_IX, NULL)) {Add_word_to_output(0x22DD);}
			else if (Expect(tok, TOKEN_TYPE_REG_IY, NULL)) {Add_word_to_output(0x22FD);}
			else {Report_error(Get_token(tok), "Expected 'a', 'hl', 'bc', 'de', 'sp', 'ix' or 'iy'"); return false;}
			Add_placeholder(token_id, expr_id, PLACEHOLDER_TYPE_WORD);
		} else {Report_error(Get_token(tok), "Expected '[bc]', '[de]', '[hl]', '[ix+*]', '[iy+*]' or [**]"); return false;}
	}
	else {Report_error(Get_token(tok), "Expected 'a', 'b', 'c', 'd', 'e', 'h', 'l', 'ixh', 'ixl', 'iyh', 'iyl' or 'sp'."); return false;}

	Next_token(tok);
	return Expect_end_of_statement(Get_token(tok));
}






//  ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^
//  | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | |
//
//
//
//
//
//
//                       HERE ENDS THE KEYWORD HANDLING HELL
//
//
//
//
//
//
//////////////////////////////////////////////////////////////////////////////////////////////// 






static bool
Parse_asm_statement(TokenIter *tok_iter) {
	Token *token = Get_token(tok_iter);
	bool result = false;
	switch (token->type) {
		case TOKEN_TYPE_KWORD_BYTE    : { result = Parse_byte_or_word(tok_iter, true);  break; } // is_byte = true
		case TOKEN_TYPE_KWORD_WORD    : { result = Parse_byte_or_word(tok_iter, false); break; } // is_byte = false
		case TOKEN_TYPE_KWORD_STRING  : { result = Parse_string_or_stringz(tok_iter, true);  break; } // is_string = true
		case TOKEN_TYPE_KWORD_STRINGZ : { result = Parse_string_or_stringz(tok_iter, false); break; } // is_string = false
		case TOKEN_TYPE_INST_JP    : { result = HANDLE_jp(tok_iter);   break; }
		case TOKEN_TYPE_INST_JR    : { result = HANDLE_jr(tok_iter);   break; }
		case TOKEN_TYPE_INST_DJNZ  : { result = HANDLE_djnz(tok_iter); break; }
		case TOKEN_TYPE_INST_DEC   : { result = Parse_inc_or_dec(tok_iter, false); break; } // is_inc = false
		case TOKEN_TYPE_INST_INC   : { result = Parse_inc_or_dec(tok_iter, true); break; }   // is_inc = true
		case TOKEN_TYPE_INST_ADD   : { result = HANDLE_add(tok_iter);  break; }
		case TOKEN_TYPE_INST_ADC   : { result = HANDLE_adc(tok_iter);  break; }
		case TOKEN_TYPE_INST_SBC   : { result = HANDLE_sbc(tok_iter);  break; }
		case TOKEN_TYPE_INST_SUB   : { result = Parse_acum_operation(tok_iter, AOP_SUB); break; }
		case TOKEN_TYPE_INST_OR    : { result = Parse_acum_operation(tok_iter, AOP_OR);  break; }
		case TOKEN_TYPE_INST_AND   : { result = Parse_acum_operation(tok_iter, AOP_AND); break; }
		case TOKEN_TYPE_INST_XOR   : { result = Parse_acum_operation(tok_iter, AOP_XOR); break; }
		case TOKEN_TYPE_INST_CP    : { result = Parse_acum_operation(tok_iter, AOP_CP);  break; }
		case TOKEN_TYPE_INST_LD    : { result = HANDLE_ld(tok_iter);   break; }
		case TOKEN_TYPE_INST_RET   : { result = HANDLE_ret(tok_iter);  break; }
		case TOKEN_TYPE_INST_CALL  : { result = HANDLE_call(tok_iter); break; }
		case TOKEN_TYPE_INST_RST   : { result = HANDLE_rst(tok_iter);  break; }
		case TOKEN_TYPE_INST_OUT   : { result = HANDLE_out(tok_iter);  break; }
		case TOKEN_TYPE_INST_IN    : { result = HANDLE_in(tok_iter);   break; }
		case TOKEN_TYPE_INST_PUSH  : { result = HANDLE_push(tok_iter); break; }
		case TOKEN_TYPE_INST_POP   : { result = HANDLE_pop(tok_iter);  break; }
		case TOKEN_TYPE_INST_EX    : { result = HANDLE_ex(tok_iter);   break; }
		case TOKEN_TYPE_INST_IM    : { result = HANDLE_im(tok_iter);   break; }
		case TOKEN_TYPE_INST_RLC   : { result = HANDLE_rlc(tok_iter);  break; }
		case TOKEN_TYPE_INST_RRC   : { result = HANDLE_rrc(tok_iter);  break; }
		case TOKEN_TYPE_INST_RL    : { result = HANDLE_rl(tok_iter);   break; }
		case TOKEN_TYPE_INST_RR    : { result = HANDLE_rr(tok_iter);   break; }
		case TOKEN_TYPE_INST_SLA   : { result = HANDLE_sla(tok_iter);  break; }
		case TOKEN_TYPE_INST_SRA   : { result = HANDLE_sra(tok_iter);  break; }
		case TOKEN_TYPE_INST_SLL   : { result = HANDLE_sll(tok_iter);  break; }
		case TOKEN_TYPE_INST_SRL   : { result = HANDLE_srl(tok_iter);  break; }
		case TOKEN_TYPE_INST_BIT   : { result = HANDLE_bit(tok_iter);  break; }
		case TOKEN_TYPE_INST_RES   : { result = HANDLE_res(tok_iter);  break; }
		case TOKEN_TYPE_INST_SET   : { result = HANDLE_set(tok_iter);  break; }
		case TOKEN_TYPE_INST_NOP   : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_DI    : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_EI    : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_EXX   : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_RLCA  : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_RRCA  : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_RLA   : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_RRA   : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_DAA   : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_CPL   : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_SCF   : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_CCF   : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_HALT  : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_RETN  : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_NEG   : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_OUTI  : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_OUTD  : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_RRD   : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_RLD   : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_LDI   : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_CPI   : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_INI   : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_LDD   : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_CPD   : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_IND   : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_LDIR  : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_CPIR  : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_INIR  : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_OTIR  : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_LDDR  : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_CPDR  : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_INDR  : { result = HANDLE_misc_instructions(tok_iter); break;}
		case TOKEN_TYPE_INST_OTDR  : { result = HANDLE_misc_instructions(tok_iter); break;}
		default :
			assert(0);
			return false;
	}
	if (!result) {
		return false;
	}
	else {
		GLOBAL.current_addr = GLOBAL.out_data_size;
		return true;
	}
}



static bool
Parse_label_or_declaration(TokenIter *tok_iter) {
	Token *token    = Get_token(tok_iter);
	assert(token->type == TOKEN_TYPE_IDENTIFIER);
	u32   token_id  = tok_iter->current;
	char *decl_str = token->value;
	u32   decl_len = token->value_len;

	Declaration d;
	Next_token(tok_iter);
	if (Expect(tok_iter, TOKEN_TYPE_COLON, NULL)) {
		d.type      = DECLARATION_TYPE_LABEL;
		d.token_id  = token_id;
		d.value     = decl_str;
		d.value_len = decl_len;
		d.item      = GLOBAL.out_data_size;
		d.result    = 0;
		d.resolved  = false;
	}
	else if (Expect(tok_iter, TOKEN_TYPE_ASSIGNMENT, NULL)) {
		Next_token(tok_iter);
		u32 expr_id = Parse_expression(tok_iter);
		if (expr_id == 0) {return false;}
		d.type      = DECLARATION_TYPE_EXPRESSION;
		d.token_id  = token_id;
		d.value     = decl_str;
		d.value_len = decl_len;
		d.item      = expr_id;
		d.result    = 0;
		d.resolved  = false;
		if (!Expect_end_of_statement(Get_token(tok_iter))) {return false;}
	}
	else {
		return false;
	}

	if (!Add_declaration(&d)) {return false;}

	return true;
}


bool
Parse_include(TokenIter *toki) {
	Next_token(toki);
	if (!Expect_or_report(toki, TOKEN_TYPE_STRING, NULL)) {return false;}
	
	Token *token_file = Get_token(toki);
	char *file_path = New_string(512);
	snprintf(file_path, 512, "%.*s", token_file->value_len, token_file->value);

	Next_token(toki);
	if (!Expect_end_of_statement(Get_token(toki))) {return false;}

	u32 f_id;
	LoadFileStatus lf_status = Load_file(file_path, token_file->file_id, &f_id);
	if (lf_status == LOAD_FILE_NOT_FOUND){
		Report_error(token_file, "Included file not found.");
		return false;
	}
	else if (lf_status == LOAD_FILE_CYCLE) {
		Report_error(token_file, "Include circular cycle detected.");
		return false;
	}
	Push_token_iter(Tokenize_file(f_id));
	return true;
}


bool
Parse_compflags(TokenIter *toki) {
	Next_token(toki);
	for (;!(Get_token(toki)->type & TOKEN_FLAG_END_OF_STATEMENT);) {
		switch (Get_token(toki)->type) {
			case TOKEN_TYPE_FLAG_START_ADDR : {
				Next_token(toki);
				if (!Token_is_or_report(Get_token(toki), TOKEN_TYPE_NUMBER, NULL)) {return false;}
				u16 w;
				if (!Token_to_word_or_report(Get_token(toki), &w)) {return false;}
				if (!GLOBAL.start_addr_overrided) {
					GLOBAL.start_addr = w;
				}
				break;
			}
			case TOKEN_TYPE_FLAG_OUT : {
				Next_token(toki);
				if (!Token_is_or_report(Get_token(toki), TOKEN_TYPE_STRING, NULL)) {return false;}
				Token *t = Get_token(toki);
				char *out_file = New_string(t->value_len+1);
				memcpy(out_file, t->value, t->value_len);
				out_file[t->value_len] = '\0';
				if (!GLOBAL.out_filename_overrided) {
					GLOBAL.out_filename = out_file;
				}
				break;
			}
			case TOKEN_TYPE_FLAG_CPC_SNA : {
				Next_token(toki);
				if (!Token_is_or_report(Get_token(toki), TOKEN_TYPE_STRING, NULL)) {return false;}
				Token *t = Get_token(toki);
				char *cpc_sna_filename = New_string(t->value_len+1);
				memcpy(cpc_sna_filename, t->value, t->value_len);
				cpc_sna_filename[t->value_len] = '\0';
				if (!GLOBAL.cpc_sna_enabled) {
					GLOBAL.cpc_sna_filename = cpc_sna_filename;
					GLOBAL.cpc_sna_enabled  = true;
				}
				break;
			}
			case TOKEN_TYPE_FLAG_NOI : {
				Next_token(toki);
				if (!Token_is_or_report(Get_token(toki), TOKEN_TYPE_STRING, NULL)) {return false;}
				Token *t = Get_token(toki);
				char *noi_filename = New_string(t->value_len+1);
				memcpy(noi_filename, t->value, t->value_len);
				noi_filename[t->value_len] = '\0';
				if (!GLOBAL.noi_enabled) {
					GLOBAL.noi_filename = noi_filename;
					GLOBAL.noi_enabled  = true;
				}
				break;
			}
			default: {
				Report_error(Get_token(toki), "Unexpected token.");
				return false;
			}
		}
		Next_token(toki);
	}
	return true;
}






bool
Resolve_declarations() {
	for (u32 i = 1; i < GLOBAL.declaration_storage_size; ++i) {
		Declaration *decl = &GLOBAL.declaration_storage[i];
		if (decl->resolved) {continue;}
		switch (decl->type) {
			case DECLARATION_TYPE_EXPRESSION:  {
				s64 result;
				if (!Compute_expression_or_report(decl->item, &result)) {return false;}
				decl->result   = result;
				decl->resolved = true;
				break;
			}
			case DECLARATION_TYPE_LABEL: {
				decl->result = decl->item + GLOBAL.start_addr;
				decl->resolved = true;
				break;
			}
			default:{
				assert(false);
			}
		}
	}
	return true;
}



bool
Resolve_placeholders() {
	for (u32 i = 1; i < GLOBAL.placeholder_storage_size; ++i) {
		Placeholder *ph = &GLOBAL.placeholder_storage[i];
		Token       *t  = &GLOBAL.token_storage[ph->token_id];
		switch (ph->type) {
			case PLACEHOLDER_TYPE_WORD: {
				u16 w;
				s64 w_s64;
				if (!Compute_expression_or_report(ph->expr_id, &w_s64)) {return false;}
				if (!S64_to_word(w_s64, &w)) {
					Report_error(t, "Value %lu overflows inside a word.", w_s64);
					return false;
				}
				Add_word_to_output_at(w, ph->address);
				break;
			}
			case PLACEHOLDER_TYPE_REL: {
				u16 source_addr = GLOBAL.start_addr + ph->address - 1;
				s64 dest_addr;
				if (!Compute_expression_or_report(ph->expr_id, &dest_addr)) {return false;}

				s64 offset = dest_addr - (source_addr + 2);

				if (offset < - 128 || offset > 127) {
					Report_error(t,"Relative jump exceds the limits (%ld), source address: 0x%04hx, destination address: 0x%04lx.", offset, source_addr, dest_addr);
					return false;
				}
				Add_byte_to_output_at(offset, ph->address);
				break;
			}
			case PLACEHOLDER_TYPE_BYTE: {
				u8  b;
				s64 b_s64;
				if (!Compute_expression_or_report(ph->expr_id, &b_s64)) {return false;}
				if (!S64_to_byte(b_s64, &b)) {
					Report_error(t, "Value %lu overflows inside a byte.", b_s64);
					return false;
				}
				Add_byte_to_output_at(b, ph->address);
				break;
			}
			default: assert(false);
		}
	}

	return true;
}



bool
Parse_args(int argc, char **argv) {
	if (argc == 1) {
		Printerr("Not enough arguments given\n");
		return false;
	}
	for (u32 i = 1; i < argc; ++i) {
		if (strcmp(argv[i], "--out") == 0) {
			++i;
			if (i >= argc) {
				Printerr(ANSI_BRED"Error: "ANSI_RST"no output file specified before "ANSI_BWHT"'--out'"ANSI_RST".\n");
				return false;
			}
			GLOBAL.out_filename = argv[i];
			GLOBAL.out_filename_overrided = true;
		}
		else if (strcmp(argv[i], "--cpc-sna") == 0) {
			++i;
			if (i >= argc) {
				Printerr(ANSI_BRED"Error: "ANSI_RST"no output sna file specified before "ANSI_BWHT"'--cpc-sna'"ANSI_RST".\n");
				return false;
			}
			GLOBAL.cpc_sna_filename = argv[i];
			GLOBAL.cpc_sna_enabled  = true;
		}
		else if (strcmp(argv[i], "--noi") == 0) {
			++i;
			if (i >= argc) {
				Printerr(ANSI_BRED"Error: "ANSI_RST"no output noi file specified before "ANSI_BWHT"'--noi'"ANSI_RST".\n");
				return false;
			}
			GLOBAL.noi_filename = argv[i];
			GLOBAL.noi_enabled  = true;
		}
		else if (strcmp(argv[i], "--start-addr") == 0) {
			++i;
			if (i >= argc) {
				Printerr(ANSI_BRED"Error: "ANSI_RST"no address specified before "ANSI_BWHT"'--start-addr'"ANSI_RST".\n");
				return false;
			}
			s64 addr;
			if (!String_to_number(argv[i], strlen(argv[i]), &addr)) {
				Printerr(ANSI_BRED"Error: "ANSI_RST"wrong format address "ANSI_BRED"'%s'"ANSI_RST".\n", argv[i]);
				return false;
			}
			if (addr > ((u16)0xFFFF)) {
				Printerr(ANSI_BRED"Error: "ANSI_RST"start address "ANSI_BRED"'0x%lx'"ANSI_RST" overflows.\n", addr);
				return false;
			}
			GLOBAL.start_addr = (u16)addr;
			GLOBAL.start_addr_overrided = true;
		}
		else {
			GLOBAL.in_filename = argv[i];
		}
	}
	if (GLOBAL.in_filename == NULL) {
		Printerr(ANSI_BRED"Error: "ANSI_RST"no input file specified.\n");
		return false;
	}
	return true;
}


#include "cpc_sna_imgs.h"

bool
Gen_cpc_sna(void) {
	static u8 sna_header[256]  = {0};
	static u8 mem_image[65536] = {0};
	
	// Copy the default header
	memcpy(sna_header, gk_sna3_header, 256);

    // Copy the default memory contents
    memcpy(&mem_image[0x0000], gk_mem0000_004F,   80);
    memcpy(&mem_image[0x4000], gk_mem4000_4BEF, 3056);
    memcpy(&mem_image[0xA670], gk_memA670_BFFF, 6544);
	
	u16 entrypoint_addr = GLOBAL.entrypoint_addr;
	u16 start_addr    =   GLOBAL.start_addr;
	u32 out_data_size =   GLOBAL.out_data_size;
	u8 *out_data      =   GLOBAL.out_data;

	// Sets the PC value
	sna_header[0x23] = 0x00FF & entrypoint_addr;
	sna_header[0x24] = entrypoint_addr >> 8;

	if (((u32)start_addr)+out_data_size > 65536) {
		fprintf(stderr, ANSI_BRED"Error:"ANSI_RST" Generated binary goes outside 64K.\n");
		return false;
	}

	if (out_data_size > 0) {
		memcpy(&mem_image[start_addr], out_data, out_data_size);
	}
	GLOBAL.cpc_sna_file = fopen(GLOBAL.cpc_sna_filename, "wb");
	if (GLOBAL.cpc_sna_file == NULL) {
		fprintf(
			stderr,
			ANSI_BRED"Error:"ANSI_RST" Cannot create file "ANSI_BWHT"'%s'"ANSI_RST".\n",
			GLOBAL.cpc_sna_filename
		);
		return false;
	}
	fwrite(sna_header, 1, sizeof(sna_header), GLOBAL.cpc_sna_file);
	fwrite(mem_image,  1, sizeof(mem_image),  GLOBAL.cpc_sna_file);
	return true;
}


bool
Gen_noi(void) {
	GLOBAL.noi_file = fopen(GLOBAL.noi_filename, "w");
	if (GLOBAL.noi_file == NULL) {
		fprintf(
			stderr,
			ANSI_BRED"Error:"ANSI_RST" Cannot create file "ANSI_BWHT"'%s'"ANSI_RST".\n",
			GLOBAL.noi_filename
		);
		return false;
	}
	for (u32 i = 1; i < GLOBAL.declaration_storage_size; ++i) {
		Declaration *decl = &GLOBAL.declaration_storage[i];
		if (decl->type == DECLARATION_TYPE_LABEL){
			assert(decl->resolved);
			fprintf(GLOBAL.noi_file, "DEF %.*s 0x%04lx\n", decl->value_len, decl->value, decl->result);
		}
	}
	return true;
}




int
main(int argc, char **argv) {
	if (!Parse_args(argc, argv)) {return -1;}
	u32 f_id;
	LoadFileStatus lf_status = Load_file(GLOBAL.in_filename, 0, &f_id);
	if (lf_status != LOAD_FILE_SUCCES){
		Printerr("Error, cannot load file \"%s\".\n", GLOBAL.in_filename);
		cleanup();
		exit(-1);
	}

	Push_token_iter(Tokenize_file(f_id));

	for (;GLOBAL.token_iter_stack_size != 0;) {
		u32 tok_iter_id     = GLOBAL.token_iter_stack_size-1;
		TokenIter *tok_iter = &GLOBAL.token_iter_stack[tok_iter_id];
		if (Get_token(tok_iter)->type == TOKEN_TYPE_EOF) {
			Pop_token_iter();
			continue;
		}
		else if (Get_token(tok_iter)->type == TOKEN_TYPE_IDENTIFIER) {
			if (!Parse_label_or_declaration(tok_iter)) { cleanup(); return -1; }
		}
		else if (Get_token(tok_iter)->type == TOKEN_TYPE_DIR_INCLUDE) {
			if (!Parse_include(tok_iter)) { cleanup(); return -1; }
		}
		else if (Get_token(tok_iter)->type == TOKEN_TYPE_DIR_COMPFLAGS) {
			if (!Parse_compflags(tok_iter)) { cleanup(); return -1; }
		}
		else if (Get_token(tok_iter)->type == TOKEN_TYPE_EOL) {
			// Skip...
		}
		else if (Get_token(tok_iter)->type & TOKEN_FLAG_ASM_STATEMENT){
			if (!Parse_asm_statement(tok_iter)) { cleanup(); return -1; }
		}
		else {
			Report_error(Get_token(tok_iter), "Unexpected token.");
			cleanup();
			return -1;
		}
		
		// The stack may be resized at this point so we cant trust tok_iter
		tok_iter = &GLOBAL.token_iter_stack[tok_iter_id];
		Next_token(tok_iter);
	}
	
	if (!Resolve_declarations()) {cleanup(); return -1;}
	if (!Resolve_placeholders()) {cleanup(); return -1;}
	
	// Finds the main
	u32 main_id = Find_declaration("main", strlen("main"));
	if (main_id == 0) {
		GLOBAL.entrypoint_addr = GLOBAL.start_addr;
	}
	else {
		Declaration *main_decl = &GLOBAL.declaration_storage[main_id];
		GLOBAL.entrypoint_addr = main_decl->result;
	}

	printf("Total tokens: %d\n", GLOBAL.token_storage_size-1);
	printf("Total files: %d\n",  GLOBAL.file_storage_size-1);
	printf("Total declarations: %d\n",  GLOBAL.declaration_storage_size-1);

	GLOBAL.out_file = fopen(GLOBAL.out_filename,"wb");
	if (GLOBAL.out_file == NULL) {
		Printerr("Error, cannot create file out.bin\n");
		cleanup();
		return -1;
	}
	fwrite(GLOBAL.out_data, 1, GLOBAL.out_data_size, GLOBAL.out_file);

	if (GLOBAL.cpc_sna_enabled) {
		if (!Gen_cpc_sna()) { cleanup(); return -1; }
	}
	
	if (GLOBAL.noi_enabled) {
		if (!Gen_noi()) {cleanup(); return -1;}
	}

	cleanup();
	return 0;
}


